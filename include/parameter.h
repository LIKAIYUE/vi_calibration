#ifndef PARAMETER_H
#define PARAMETER_H

#include<fstream>
#include<Eigen/Dense>

using namespace Eigen;
using namespace std;

extern ofstream file_info;

const double camera_front_rx = 1.927783;
const double camera_front_ry = 0.026793;
const double camera_front_rz = -0.017847;
const double camera_front_tx = -0.111373;
const double camera_front_ty = 2.012805;
const double camera_front_tz = -3.418632;


// const double ACC_N = 0.01;
// const double GYR_N = 0.005;
// const double ACC_W = 0.02;
// const double GYR_W = 4.0e-3;

// const double ACC_N = 0.01;
// const double GYR_N = 0.005;
// const double ACC_W = 0.007;
// const double GYR_W = 4.0e-4;

const double ACC_N = 0.01;
const double GYR_N = 0.005;
const double ACC_W = 0.0002;
const double GYR_W = 4.0e-6;

//euroc
// const double ACC_N = 0.002;
// const double GYR_N = 1.6968e-04;
// const double ACC_W = 0.003;
// const double GYR_W = 1.9393e-05;

const Vector3d G(0.0, 0.0, 9.794); 

const double time_stamp_to_sec = 1000000000.0;

const int WINDOW_SIZE = 20;

enum StateOrder
{
    O_P = 0,
    O_R = 3,
    O_V = 6,
    O_BA = 9,
    O_BG = 12
};

#endif