#ifndef SLIDINGWINDOW_H
#define SLIDINGWINDOW_H

#include<vector>
#include"ImageProcessor.h"

using namespace std;

class SlidingWindow{
public:
    const int WINDOW_SIZE = 20;

    vector<Frame> frames;

    void updateWindow(Frame new_frame);

    void Optimize();
};



#endif