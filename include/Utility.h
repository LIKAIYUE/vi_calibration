#ifndef UTILITY_H
#define UTILITY_H

#include<Eigen/Dense>

using namespace Eigen;

Quaterniond Theta2Q(Vector3d deltaTheta);

Matrix<double, 3, 3> skewSymmetric(Vector3d q);

Matrix<double, 4, 4> Qleft(Quaterniond qq);

Matrix<double, 4, 4> Qright(Quaterniond pp);



#endif 