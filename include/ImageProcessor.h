#ifndef IMAGEPRECESSOR_H
#define IMAGEPRECESSOR_H

#include<Eigen/Dense>
#include<string>

#include"IMU/IMUBase.h"

using namespace Eigen;
using namespace std;

struct Frame{
    double timestamp;
    Matrix3d R;
    Vector3d P;
    Vector3d velocity;

    IMUProcess imu_process;
};


class ImageProcessor{
public:
    vector<Frame> frames;

    void LoadFrames(string path);

    void LoadGroundTruth(string path);
};

#endif