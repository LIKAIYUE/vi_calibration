#ifndef INITIALIZER_H
#define INITIALIZER_H

#include<Eigen/Dense>
#include<vector>
#include<fstream>

#include"ImageProcessor.h"

using namespace std;
using namespace Eigen;

class Initializer
{
public:
    const int MIN_FRAME_NUM = 20;

    Matrix3d Rbc;
    Vector3d first_Pbc, Pbc;
    
    Vector3d ba, bg;
    double s;
    Vector3d first_g, g;
    
    vector<Frame> frames;

    vector<Matrix3d> Rc_g;

    Initializer();

    void addFrame(Frame frame);

    bool CheckIMUObservibility();

    bool computeRbc();

    void computeBg();

    bool computeGravityVelocity();

    bool computeGravityVelocityPbc();

    bool computeGravityVelocityScale();

    bool computeGravityVelocityPbcScale();

    bool ComputeGravityScale_VIORB();

    bool ComputeGravityPcbScale_VIORB();

    void RefineGravityVelocity();

    void RefineGravityVelocityPbc();

    bool initialize();

};

#endif