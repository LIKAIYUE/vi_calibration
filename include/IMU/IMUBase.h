#ifndef IMUBASE_H
#define IMUBASE_H

#include<Eigen/Dense>
#include<vector>

using namespace std;
using namespace Eigen;

struct IMUData{
    double timestamp;
    Vector3d acc, gyr;
};

// IMUData::IMUData(IMUData &imudata)
// {
//     timestamp = imudata.timestamp;
//     ax = imudata.ax;
//     ay = imudata.ay;
//     az = imudata.az;
//     wx = imudata.wx;
//     wy = imudata.wy;
//     wz = imudata.wz;
// }

struct PreIntegration{
    Vector3d delta_p;
    Quaterniond delta_q;
    Vector3d delta_v;

    Matrix<double, 15, 15> jacobian, covariance;
    Matrix<double, 15, 15> step_jacobian;
    Matrix<double, 18, 18> noise;
};


class IMUProcess{
public:
    vector<IMUData> imu_data;

    Vector3d ba, bg;

    PreIntegration pre_integration;

    IMUProcess();

    IMUProcess(Vector3d ba, Vector3d bg);

    void PreIntegrate();

    void UpdatePreIntegration(Vector3d ba, Vector3d bg);

};

#endif

