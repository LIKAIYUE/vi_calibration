#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include<vector>
#include<iostream>
#include<fstream>
#include"ImageProcessor.h"
#include"Utility.h"
#include"parameter.h"
#include"IMUBase.h"

using namespace std;

struct CostFunctor
{
    CostFunctor(Frame _frame_curr, Frame _frame_next, Vector3d _g): frame_curr(_frame_curr), frame_next(_frame_next), g(_g){};

    bool operator()(const double* const param_qbc, const double* const param_pcb, 
                    const double* const param_v_curr, const double* const param_ba_curr, const double* const param_bg_curr,
                    const double* const param_v_next, const double* const param_ba_next, const double* const param_bg_next, double* residuals) const{
        Quaterniond qbc(param_qbc[0], param_qbc[1], param_qbc[2], param_qbc[3]);    
        Vector3d pcb(param_pcb[0], param_pcb[1], param_pcb[2]);

        Vector3d v_curr(param_v_curr[0], param_v_curr[1], param_v_curr[2]);
        Vector3d ba_curr(param_ba_curr[0], param_ba_curr[1], param_ba_curr[2]);
        Vector3d bg_curr(param_bg_curr[0], param_bg_curr[1], param_bg_curr[2]);

        Vector3d v_next(param_v_next[0], param_v_next[1], param_v_next[2]);
        Vector3d ba_next(param_ba_next[0], param_ba_next[1], param_ba_next[2]);
        Vector3d bg_next(param_bg_next[0], param_bg_next[1], param_bg_next[2]);

        Matrix3d R_curr = frame_curr.R;
        Matrix3d R_next = frame_next.R;
        Vector3d P_curr = frame_curr.P;
        Vector3d P_next = frame_next.P;
        double dt = frame_next.timestamp - frame_curr.timestamp;

        Matrix3d dq_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
        Matrix3d dv_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);
        Matrix3d dv_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);
        Matrix3d dp_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);
        Matrix3d dp_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);

        Quaterniond correct_q = frame_curr.imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg_curr - frame_curr.imu_process.bg));
        Vector3d correct_v = frame_curr.imu_process.pre_integration.delta_v + dv_dbg * (bg_curr - frame_curr.imu_process.bg) + dv_dba * (ba_curr - frame_curr.imu_process.ba);
        Vector3d correct_p = frame_curr.imu_process.pre_integration.delta_p + dp_dbg * (bg_curr - frame_curr.imu_process.bg) + dp_dba * (ba_curr - frame_curr.imu_process.ba);

        Vector3d residual_p = qbc * R_curr.transpose() * (P_next + R_next * pcb - P_curr - R_curr * pcb + 0.5 * g * dt * dt - v_curr * dt) - correct_p;
        Vector3d residual_v = qbc * R_curr.transpose() * (v_next + g * dt - v_curr) - correct_v;
        Vector3d residual_q = 2 * (correct_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();
        Vector3d residual_ba = ba_next - ba_curr;
        Vector3d residual_bg = bg_next - bg_curr;

        Map<Matrix<double, 15, 1>> residual_all(residuals);
        residual_all.block<3, 1>(0, 0) = residual_p;
        residual_all.block<3, 1>(3, 0) = residual_q;
        residual_all.block<3, 1>(6, 0) = residual_v;
        residual_all.block<3, 1>(9, 0) = residual_ba;
        residual_all.block<3, 1>(12, 0) = residual_bg;

        
        //cout << "residual_all = " <<residual_all.transpose() << endl;

        Matrix<double ,15 ,15> covariance = frame_curr.imu_process.pre_integration.covariance;
        //cout << "covarance = " << covariance << endl;
        //covariance.setIdentity();
        //covariance.block<6, 6>(0, 0) = Matrix<double, 6, 6>::Identity() * 0.01;
        // covariance(0, 0) = 1e-20;
        // covariance(1, 1) = 1e-20;
        // covariance(2, 2) = 1e-20;
        Matrix<double, 15, 15> sqrt_info = LLT<Matrix<double, 15, 15>>(covariance.inverse()).matrixL().transpose();
        //sqrt_info.setIdentity();
        //cout << " sqrt_info " << sqrt_info << endl;
        residual_all = sqrt_info * residual_all;

        return true;
    }
    Vector3d g;
    Frame frame_curr, frame_next;
};
struct CostFunctor_Test
{
    CostFunctor_Test(Frame _frame_curr, Frame _frame_next, Vector3d _g): frame_curr(_frame_curr), frame_next(_frame_next), g(_g){};

    bool operator()(const double* const param_qbc, const double* const param_pcb, const double* const param_v_curr, const double* const param_ba_curr, const double* const param_bg_curr, const double* const param_v_next, double* residuals) const{
        Quaterniond qbc(param_qbc[0], param_qbc[1], param_qbc[2], param_qbc[3]);    
        Vector3d pcb(param_pcb[0], param_pcb[1], param_pcb[2]);

        Vector3d v_curr(param_v_curr[0], param_v_curr[1], param_v_curr[2]);
        Vector3d ba_curr(param_ba_curr[0], param_ba_curr[1], param_ba_curr[2]);
        Vector3d bg_curr(param_bg_curr[0], param_bg_curr[1], param_bg_curr[2]);

        Vector3d v_next(param_v_next[0], param_v_next[1], param_v_next[2]);

        Matrix3d R_curr = frame_curr.R;
        Matrix3d R_next = frame_next.R;
        Vector3d P_curr = frame_curr.P;
        Vector3d P_next = frame_next.P;
        double dt = frame_next.timestamp - frame_curr.timestamp;

        Matrix3d dq_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
        Matrix3d dv_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);
        Matrix3d dv_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);
        Matrix3d dp_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);
        Matrix3d dp_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);

        Quaterniond correct_q = frame_curr.imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg_curr - frame_curr.imu_process.bg));
        Vector3d correct_v = frame_curr.imu_process.pre_integration.delta_v + dv_dbg * (bg_curr - frame_curr.imu_process.bg) + dv_dba * (ba_curr - frame_curr.imu_process.ba);
        Vector3d correct_p = frame_curr.imu_process.pre_integration.delta_p + dp_dbg * (bg_curr - frame_curr.imu_process.bg) + dp_dba * (ba_curr - frame_curr.imu_process.ba);

        Vector3d residual_p = qbc * R_curr.transpose() * (P_next + R_next * pcb - P_curr - R_curr * pcb + 0.5 * g * dt * dt - v_curr * dt) - correct_p;
        Vector3d residual_v = qbc * R_curr.transpose() * (v_next + g * dt - v_curr) - correct_v;
        Vector3d residual_q = 2 * (correct_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();

        Map<Matrix<double, 9, 1>> residual_all(residuals);
        residual_all.block<3, 1>(0, 0) = residual_p;
        residual_all.block<3, 1>(3, 0) = residual_q;
        residual_all.block<3, 1>(6, 0) = residual_v;

        
        //cout << "residual_all = " <<residual_all.transpose() << endl;

        // Matrix<double ,15 ,15> covariance = frame_curr.imu_process.pre_integration.covariance;
        // Matrix<double, 15, 15> sqrt_info = LLT<Matrix<double, 15, 15>>(covariance.inverse()).matrixL().transpose();
        
        Matrix<double, 9, 9> sqrt_info;
        sqrt_info.setIdentity();

        residual_all = sqrt_info * residual_all;

        return true;
    }
    Vector3d g;
    Frame frame_curr, frame_next;
};


struct CostFunctor_NoVelocity
{
    CostFunctor_NoVelocity(Frame _frame_curr, Frame _frame_next, Vector3d _g): frame_curr(_frame_curr), frame_next(_frame_next), g(_g){};

    bool operator()(const double* const param_qbc, const double* const param_pcb, 
                    const double* const param_ba_curr, const double* const param_bg_curr,
                    const double* const param_ba_next, const double* const param_bg_next, double* residuals) const{
        Quaterniond qbc(param_qbc[0], param_qbc[1], param_qbc[2], param_qbc[3]);    
        Vector3d pcb(param_pcb[0], param_pcb[1], param_pcb[2]);

        Vector3d ba_curr(param_ba_curr[0], param_ba_curr[1], param_ba_curr[2]);
        Vector3d bg_curr(param_bg_curr[0], param_bg_curr[1], param_bg_curr[2]);

        Vector3d ba_next(param_ba_next[0], param_ba_next[1], param_ba_next[2]);
        Vector3d bg_next(param_bg_next[0], param_bg_next[1], param_bg_next[2]);

        Vector3d V_curr = frame_curr.velocity;
        Vector3d V_next = frame_next.velocity;
        Matrix3d R_curr = frame_curr.R;
        Matrix3d R_next = frame_next.R;
        Vector3d P_curr = frame_curr.P;
        Vector3d P_next = frame_next.P;
        double dt = frame_next.timestamp - frame_curr.timestamp;

        Matrix3d dq_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);

        Matrix3d dv_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);
        Matrix3d dv_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);

        Matrix3d dp_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);
        Matrix3d dp_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);

        Quaterniond correct_q = frame_curr.imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg_curr - frame_curr.imu_process.bg));
        Vector3d correct_v = frame_curr.imu_process.pre_integration.delta_v + dv_dbg * (bg_curr - frame_curr.imu_process.bg) + dv_dba * (ba_curr - frame_curr.imu_process.ba);
        Vector3d correct_p = frame_curr.imu_process.pre_integration.delta_p + dp_dbg * (bg_curr - frame_curr.imu_process.bg) + dp_dba * (ba_curr - frame_curr.imu_process.ba);

        Vector3d residual_p = qbc * R_curr.transpose() * (P_next + R_next * pcb - P_curr - R_curr * pcb + 0.5 * g * dt * dt - V_curr * dt) - correct_p;
        Vector3d residual_v = qbc * R_curr.transpose() * (V_next + g * dt - V_curr) - correct_v;
        Vector3d residual_q = 2 * (correct_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();
        Vector3d residual_ba = ba_next - ba_curr;
        Vector3d residual_bg = bg_next - bg_curr;

        Map<Matrix<double, 15, 1>> residual_all(residuals);
        residual_all.block<3, 1>(0, 0) = residual_p;
        residual_all.block<3, 1>(3, 0) = residual_q;
        residual_all.block<3, 1>(6, 0) = residual_v;
        residual_all.block<3, 1>(9, 0) = residual_ba;
        residual_all.block<3, 1>(12, 0) = residual_bg;

        //cout << residual_all.transpose() << endl;

        Matrix<double ,15 ,15> covariance = frame_curr.imu_process.pre_integration.covariance;
        //covariance.setIdentity();
        covariance(0, 0) = 1e-20;
        covariance(1, 1) = 1e-20;
        covariance(2, 2) = 1e-20;
        Matrix<double, 15, 15> sqrt_info = LLT<Matrix<double, 15, 15>>(covariance.inverse()).matrixL().transpose();
        
        residual_all = sqrt_info * residual_all;

        return true;
    }
    Vector3d g;
    Frame frame_curr, frame_next;
};

struct CostFunctor_Rbc1
{
    CostFunctor_Rbc1(IMUProcess _imu_process, Matrix3d _R_curr, Matrix3d _R_next): imu_process(_imu_process), R_curr(_R_curr), R_next(_R_next){}

    bool operator()(const double* const param_qbc, const double* const param_next_bg, double* residuals) const{
        Quaterniond qbc(param_qbc[0], param_qbc[1], param_qbc[2], param_qbc[3]);
        Vector3d curr_bg = imu_process.bg;
        Vector3d next_bg(param_next_bg[0], param_next_bg[1], param_next_bg[2]);

        Vector3d residual_theta = 2 * (imu_process.pre_integration.delta_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();
        
        //fixed the first frame's R
        //Vector3d residual_theta = 2 * (imu_process.pre_integration.delta_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr)).vec();

        Vector3d dbg = next_bg - curr_bg;

        Matrix<double, 6, 1> residual;
        residual << residual_theta(0), residual_theta(1), residual_theta(2), dbg(0), dbg(1), dbg(2);

        Matrix<double, 6, 6> info;
        info.setZero();
        info.block<3, 3>(0, 0) = imu_process.pre_integration.covariance.block<3, 3>(O_R, O_R);
        info.block<3, 3>(3, 3) = imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_BG);
        // info.block<3, 3>(0, 3) = imu_process.pre_integration.covariance.block<3, 3>(O_R, O_BG);
        // info.block<3, 3>(3, 0) = imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_R);
        
        Matrix<double, 6, 6> sqrt_info = LLT<Matrix<double, 6, 6>>(info.inverse()).matrixL().transpose();
        //sqrt_info.setIdentity();
        residual = sqrt_info * residual;

        residuals[0] = residual(0);
        residuals[1] = residual(1);
        residuals[2] = residual(2);
        residuals[3] = residual(3);
        residuals[4] = residual(4);
        residuals[5] = residual(5);

        return true;
    }

    IMUProcess imu_process;
    Matrix3d R_curr, R_next;

};


struct CostFunctor_Rbc
{
    CostFunctor_Rbc(IMUProcess _imu_process, Matrix3d _R_curr, Matrix3d _R_next): imu_process(_imu_process), R_curr(_R_curr), R_next(_R_next){}

    bool operator()(const double* const param_qbc, const double* const param_curr_bg, const double* const param_next_bg, double* residuals) const{
        Quaterniond qbc(param_qbc[0], param_qbc[1], param_qbc[2], param_qbc[3]);
        Vector3d curr_bg(param_curr_bg[0], param_curr_bg[1], param_curr_bg[2]);
        Vector3d next_bg(param_next_bg[0], param_next_bg[1], param_next_bg[2]);

        Matrix3d dq_dbg = imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
        Quaterniond correct_q = imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (curr_bg - imu_process.bg));

        //Matrix3d delta_R = correct_q * qbc * R_next.transpose() * R_curr * qbc.toRotationMatrix().transpose();
        //Vector3d residual_theta = 2 * Quaterniond(delta_R).vec();
        
        //重点！！！当用这个优化时，qbc跑偏非常严重
        Vector3d residual_theta = 2 * (correct_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();
        
        Vector3d dbg = next_bg - curr_bg;
        // residuals[3] = dbg(0);
        // residuals[4] = dbg(1);
        // residuals[5] = dbg(2);

        Matrix<double, 6, 1> residual;
        residual << residual_theta(0), residual_theta(1), residual_theta(2), dbg(0), dbg(1), dbg(2);
        
        Matrix<double, 6, 6> info;
        info.setIdentity();
        //info.block<3, 3>(0, 0) = Matrix3d::Identity() * 1e-20;
        info.block<3, 3>(0, 0) = imu_process.pre_integration.covariance.block<3, 3>(O_R, O_R);
        info.block<3, 3>(3, 3) = imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_BG);
        // info.block<3, 3>(0, 3) = imu_process.pre_integration.covariance.block<3, 3>(O_R, O_BG);
        // info.block<3, 3>(3, 0) = imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_R);
        
        Matrix<double, 6, 6> sqrt_info = LLT<Matrix<double, 6, 6>>(info.inverse()).matrixL().transpose();
        //cout << "sqrt_info=" << sqrt_info << endl;
        //sqrt_info.setIdentity();
        residual = sqrt_info * residual;
        //cout << "sqrt_info = " << sqrt_info << endl;
        residuals[0] = residual(0);
        residuals[1] = residual(1);
        residuals[2] = residual(2);
        residuals[3] = residual(3);
        residuals[4] = residual(4);
        residuals[5] = residual(5);
        
        file_info << "residual_theta = " << residual_theta.transpose() << endl;
        //cout << "residual_theta = " << residual_theta.transpose() << endl; 
        //cout << "qbc = " << qbc.w() << qbc.x() << qbc.y() << qbc.z() << endl;
        //cout << "residual = " << residual.transpose() << endl;

        return true;
    }

    IMUProcess imu_process;
    Matrix3d R_curr, R_next;
};


struct CostFunctor_Rbc_Test
{
    CostFunctor_Rbc_Test(IMUProcess _imu_process, Matrix3d _R_curr, Matrix3d _R_next, Quaterniond _qbc): imu_process(_imu_process), R_curr(_R_curr), R_next(_R_next), qbc(_qbc){}

    bool operator()(const double* const param_curr_bg, double* residuals) const{
        Vector3d curr_bg(param_curr_bg[0], param_curr_bg[1], param_curr_bg[2]);

        Matrix3d dq_dbg = imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
        Quaterniond correct_q = imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (curr_bg - imu_process.bg));

        Vector3d residual_theta = 2 * (correct_q * qbc * Quaterniond(R_next.transpose()) * Quaterniond(R_curr) * qbc.inverse()).vec();

        residuals[0] = residual_theta(0);
        residuals[1] = residual_theta(1);
        residuals[2] = residual_theta(2);

        return true;
    }

    IMUProcess imu_process;
    Matrix3d R_curr, R_next;
    Quaterniond qbc;
};


class Optimizer{
public:
    //the marginlization type
    int flag;

    vector<Frame> frames;

    Matrix3d Rbc;
    Vector3d Pcb;

    Vector3d g;

    vector<Vector3d> v, ba, bg;

    Optimizer(vector<Frame> frames, int start, int end, Matrix3d Rbc, Vector3d Pbc, Vector3d g);

    void UpdateWindow(Frame frame, int flag);

    Matrix<double, 15, 1> Evaluate(Frame frame, Frame frame_next, Vector3d V_i, Vector3d Ba_i, Vector3d Bg_i, Vector3d v_j, Vector3d Ba_j, Vector3d Bg_j);
    
    void Optimize();

    void OptimizeRbc();

    void Optimize_NoVelocity_Ceres();
    
    void Optimize_Ceres(int flag);
    
    void Optimize_Ceres_Test();

    void OptimizeRbc_Ceres();

    void OptimizeRbc_Ceres_Test();

    void Optimize_Ceres_before();
};

#endif