#include"IMUBase.h"
#include"parameter.h"
#include<iostream>
#include<iomanip>

using namespace std;

IMUProcess::IMUProcess()
{
    ba << 0.0, 0.0, 0.0;
    bg << 0.0, 0.0, 0.0;

    pre_integration.jacobian.setIdentity();
    pre_integration.covariance.setZero();

    pre_integration.delta_p.setZero();
    pre_integration.delta_v.setZero();
    pre_integration.delta_q.setIdentity();

    pre_integration.noise.setZero();

    pre_integration.noise.block<3, 3>(0, 0) =  (ACC_N * ACC_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(3, 3) =  (GYR_N * GYR_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(6, 6) =  (ACC_N * ACC_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(9, 9) =  (GYR_N * GYR_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(12, 12) =  (ACC_W * ACC_W) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(15, 15) =  (GYR_W * GYR_W) * Eigen::Matrix3d::Identity();
}

IMUProcess::IMUProcess(Vector3d ba, Vector3d bg)
{
    this -> ba = ba;
    this -> bg = bg;

    pre_integration.jacobian.setIdentity();
    pre_integration.covariance.setZero();

    pre_integration.delta_p.setZero();
    pre_integration.delta_v.setZero();
    pre_integration.delta_q.setIdentity();


    pre_integration.noise.setZero();

    pre_integration.noise.block<3, 3>(0, 0) =  (ACC_N * ACC_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(3, 3) =  (GYR_N * GYR_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(6, 6) =  (ACC_N * ACC_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(9, 9) =  (GYR_N * GYR_N) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(12, 12) =  (ACC_W * ACC_W) * Eigen::Matrix3d::Identity();
    pre_integration.noise.block<3, 3>(15, 15) =  (GYR_W * GYR_W) * Eigen::Matrix3d::Identity();
}

void IMUProcess::PreIntegrate()
{
    Vector3d delta_p, delta_v;
    Quaterniond delta_q;
    //important
    delta_p.setZero();
    delta_v.setZero();
    delta_q.setIdentity();
    cout << "IMUBase.cpp : PreIntegrate() : imu_data.size = " << imu_data.size() << endl;
    for(int i = 0;i < imu_data.size() - 1;i++)
    {
        double dt = imu_data[i + 1].timestamp - imu_data[i].timestamp;
        Vector3d gyr_middle = 0.5 * (imu_data[i].gyr + imu_data[i + 1].gyr) - bg;
        Quaterniond delta_q_next = delta_q * Quaterniond(1, 0.5 * gyr_middle(0) * dt, 0.5 * gyr_middle(1) * dt, 0.5 * gyr_middle(2) * dt);
        Vector3d acc_middle = 0.5 * (delta_q * (imu_data[i].acc - ba) + delta_q_next * (imu_data[i + 1].acc - ba));

        delta_p = delta_p + delta_v * dt + 0.5 * acc_middle * dt * dt;
        delta_v = delta_v + acc_middle * dt;
        
        // cout << "dt" << dt << endl;
        // cout << "acc_middle = " << acc_middle << endl;
        // cout << "delta_p = " << delta_p << endl;
        // cout << "delta_v = " << delta_v << endl;
        // cout << "delta_q = " << delta_q.vec() << endl;
        // cout << endl;
        
        //update_jacobian
        Vector3d w_x = gyr_middle;
        Vector3d a_0_x = imu_data[i].acc - ba;
        Vector3d a_1_x = imu_data[i + 1].acc - ba;
        Matrix3d R_w_x, R_a_0_x, R_a_1_x;

        R_w_x<<0, -w_x(2), w_x(1),
            w_x(2), 0, -w_x(0),
            -w_x(1), w_x(0), 0;
        R_a_0_x<<0, -a_0_x(2), a_0_x(1),
            a_0_x(2), 0, -a_0_x(0),
            -a_0_x(1), a_0_x(0), 0;
        R_a_1_x<<0, -a_1_x(2), a_1_x(1),
            a_1_x(2), 0, -a_1_x(0),
            -a_1_x(1), a_1_x(0), 0;

        MatrixXd F = MatrixXd::Zero(15, 15);
        F.block<3, 3>(0, 0) = Matrix3d::Identity();
        F.block<3, 3>(0, 3) = -0.25 * delta_q.toRotationMatrix() * R_a_0_x * dt * dt + 
                                -0.25 * delta_q_next.toRotationMatrix() * R_a_1_x * (Matrix3d::Identity() - R_w_x * dt) * dt * dt;
        F.block<3, 3>(0, 6) = MatrixXd::Identity(3,3) * dt;
        F.block<3, 3>(0, 9) = -0.25 * (delta_q.toRotationMatrix() + delta_q_next.toRotationMatrix()) * dt * dt;
        F.block<3, 3>(0, 12) = -0.25 * delta_q_next.toRotationMatrix() * R_a_1_x * dt * dt * -dt;
        F.block<3, 3>(3, 3) = Matrix3d::Identity() - R_w_x * dt;
        F.block<3, 3>(3, 12) = -1.0 * MatrixXd::Identity(3,3) * dt;
        F.block<3, 3>(6, 3) = -0.5 * delta_q.toRotationMatrix() * R_a_0_x * dt + 
                                -0.5 * delta_q_next.toRotationMatrix() * R_a_1_x * (Matrix3d::Identity() - R_w_x * dt) * dt;
        F.block<3, 3>(6, 6) = Matrix3d::Identity();
        F.block<3, 3>(6, 9) = -0.5 * (delta_q.toRotationMatrix() + delta_q_next.toRotationMatrix()) * dt;
        F.block<3, 3>(6, 12) = -0.5 * delta_q_next.toRotationMatrix() * R_a_1_x * dt * -dt;
        F.block<3, 3>(9, 9) = Matrix3d::Identity();
        F.block<3, 3>(12, 12) = Matrix3d::Identity();
        //cout<<"A"<<endl<<A<<endl;

        MatrixXd V = MatrixXd::Zero(15,18);
        V.block<3, 3>(0, 0) =  0.25 * delta_q.toRotationMatrix() * dt * dt;
        V.block<3, 3>(0, 3) =  0.25 * -delta_q_next.toRotationMatrix() * R_a_1_x  * dt * dt * 0.5 * dt;
        V.block<3, 3>(0, 6) =  0.25 * delta_q_next.toRotationMatrix() * dt * dt;
        V.block<3, 3>(0, 9) =  V.block<3, 3>(0, 3);
        V.block<3, 3>(3, 3) =  0.5 * MatrixXd::Identity(3,3) * dt;
        V.block<3, 3>(3, 9) =  0.5 * MatrixXd::Identity(3,3) * dt;
        V.block<3, 3>(6, 0) =  0.5 * delta_q.toRotationMatrix() * dt;
        V.block<3, 3>(6, 3) =  0.5 * -delta_q_next.toRotationMatrix() * R_a_1_x  * dt * 0.5 * dt;
        V.block<3, 3>(6, 6) =  0.5 * delta_q_next.toRotationMatrix() * dt;
        V.block<3, 3>(6, 9) =  V.block<3, 3>(6, 3);
        V.block<3, 3>(9, 12) = MatrixXd::Identity(3,3) * dt;
        V.block<3, 3>(12, 15) = MatrixXd::Identity(3,3) * dt;
        
        delta_q = delta_q_next;

        //step_jacobian = F;
        //step_V = V;
        pre_integration.jacobian = F * pre_integration.jacobian;
        pre_integration.covariance = F * pre_integration.covariance * F.transpose() + V * pre_integration.noise * V.transpose();

        //cout << "V = "<< scientific << V << endl;
        //cout << "IMUBase.cpp : PreIntegrate() : pre_integration.covariance = " << scientific << pre_integration.covariance.block<1, 15>(0, 0) << endl;
        //cout << "                             : pre_integration.noise = " << scientific << pre_integration.noise(0, 0) << pre_integration.noise(1, 1) << pre_integration.noise(2, 2) << endl;
    }

    this -> pre_integration.delta_p = delta_p;
    this -> pre_integration.delta_q = delta_q;
    this -> pre_integration.delta_v = delta_v;

    // cout << "IMUBase.cpp : PreIntegrate() : delta_v" << delta_v.transpose() << endl;
}


void IMUProcess::UpdatePreIntegration(Vector3d ba, Vector3d bg)
{
    this -> ba = ba;
    this -> bg = bg;

    pre_integration.jacobian.setIdentity();
    pre_integration.covariance.setZero();
    
    pre_integration.delta_p.setZero();
    pre_integration.delta_v.setZero();
    pre_integration.delta_q.setIdentity();
    
    PreIntegrate();
}