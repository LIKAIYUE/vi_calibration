#include"SlidingWindow.h"

void SlidingWindow::updateWindow(Frame new_frame)
{
    if(frames.size() < WINDOW_SIZE)
        frames.push_back(new_frame);
    else
    {
        //marginalization
        for(int i = 0;i < WINDOW_SIZE + 1;i++)
        {
            frames[i] = frames[i + 1];
        }
        frames[WINDOW_SIZE - 1] = new_frame;
    }
    
}