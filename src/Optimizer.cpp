#include"Optimizer.h"
#include"parameter.h"
#include"Utility.h"
#include"rotation_local_parameterization.h"
#include<iostream>
#include<stdio.h>
#include<iomanip>
#include<ceres/ceres.h>

using namespace std;

Optimizer::Optimizer(vector<Frame> _frames, int start, int end, Matrix3d Rbc, Vector3d Pbc, Vector3d g)
{
    for(int i = start;i <= end;i++)
    {
        this -> frames.push_back(_frames[i]);
    }

    for(int i = 0;i < frames.size();i++)
    {
        v.push_back(this -> frames[i].velocity);
        ba.push_back(this -> frames[i].imu_process.ba);
        bg.push_back(this -> frames[i].imu_process.bg);
    }

    this -> Rbc = Rbc;
    this -> Pcb = -Rbc.transpose() * Pbc;
    this -> g = g;
}

void Optimizer::UpdateWindow(Frame frame, int _flag)
{
    flag = _flag;
    
    if(flag == 1)
    {
            //update the last second frame's pre_integration
            Frame last_frame = frames[WINDOW_SIZE - 1];

            for(int k = 1;k < last_frame.imu_process.imu_data.size();k++)
            {
                frames[WINDOW_SIZE - 2].imu_process.imu_data.push_back(last_frame.imu_process.imu_data[k]);
            }

            Vector3d ba = frames[WINDOW_SIZE - 2].imu_process.ba;
            Vector3d bg = frames[WINDOW_SIZE - 2].imu_process.bg;
            frames[WINDOW_SIZE - 2].imu_process.UpdatePreIntegration(ba, bg);
            
            frames.pop_back();
            frames.push_back(frame);
    }
    else
    {
        frames.push_back(frame);
    }
    

}

void Optimizer::Optimize()
{
    double pre_residual = 1e20;
    while(true)
    {
        int length = frames.size() * 9 + 6;

        vector<MatrixXd> Jacobians(frames.size() - 1);
        vector<Matrix<double, 15, 1>> residuals(frames.size() - 1);
        

        // cout << frames[frames.size() - 1].R << endl;
        // cout << frames[frames.size() - 1].imu_process.pre_integration.covariance << endl;
        // return ;

        for(int i = 0;i < frames.size() - 1;i++)
        {
            Eigen::Matrix<double, 15, 15> sqrt_info = Eigen::LLT<Eigen::Matrix<double, 15, 15>>(frames[i].imu_process.pre_integration.covariance.inverse()).matrixL().transpose();
            //sqrt_info.setIdentity();
            //cout << frames[i].imu_process.pre_integration.covariance << endl;
            residuals[i] = Evaluate(frames[i], frames[i + 1], v[i], ba[i], bg[i], v[i + 1], ba[i + 1],bg[i + 1]);
            //cout << "residuals = " << residuals[i] << endl;
            residuals[i] = sqrt_info * residuals[i];

            Matrix3d dp_dba = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);
            Matrix3d dp_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);

            Matrix3d dq_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);

            Matrix3d dv_dba = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);
            Matrix3d dv_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);

            //cout << frames[i].imu_process.pre_integration.jacobian << endl;

            Quaterniond correct_dq = frames[i].imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg[i] - frames[i].imu_process.bg)); 

            Jacobians[i].resize(15, length);
            Jacobians[i].setZero();

            //v_i ba_i bg_i
            double dt = frames[i + 1].timestamp - frames[i].timestamp;
            Jacobians[i].block<3 ,3>(0, i * 9) = -Rbc * frames[i].R.transpose() * dt;
            Jacobians[i].block<3, 3>(3, i * 9) = -frames[i].R.transpose();

            Jacobians[i].block<3, 3>(0, i * 9 + 3) = -dp_dba;
            Jacobians[i].block<3, 3>(3, i * 9 + 3) = -dv_dba;
            Jacobians[i].block<3, 3>(9, i * 9 + 3) = -Matrix3d::Identity();
                
            Jacobians[i].block<3, 3>(0, i * 9 + 6) = -dp_dbg;
            Jacobians[i].block<3 ,3>(3, i * 9 + 6) = -dv_dbg;

            Quaterniond q_i = Quaterniond(frames[i].R * Rbc.transpose());
            Quaterniond q_j_inv = Quaterniond(Rbc * frames[i + 1].R.transpose());
            Jacobians[i].block<3, 3>(6, i * 9 + 6) = (Qright(q_j_inv * q_i) * Qleft(correct_dq)).bottomRightCorner<3, 3>() * dq_dbg; //*****
            Jacobians[i].block<3 ,3>(12, i * 9 + 6) = -Matrix3d::Identity();

            // if(i == 9)
            //     cout << "+++++" << endl;

            //v_i+1 ba_i+1 bg_i+1
            Jacobians[i].block<3, 3>(3, i * 9 + 9) = Rbc * frames[i].R.transpose();
            Jacobians[i].block<3, 3>(9, i * 9 + 12) = Matrix3d::Identity();
            Jacobians[i].block<3, 3>(12, i * 9 + 15) = Matrix3d::Identity();

            //Rbc
            Jacobians[i].block<3, 3>(0, length - 6) = -Rbc * skewSymmetric(frames[i].R.transpose() * (frames[i + 1].P + frames[i + 1].R * Pcb - frames[i].P - frames[i].R * Pcb + 0.5 * g * dt - v[i] * dt));
            Jacobians[i].block<3, 3>(3, length - 6) = -Rbc * skewSymmetric(frames[i].R.transpose() * (v[i + 1] - v[i] - 0.5 * g * dt));
            // if(i == 9)
            //     cout << "+++++" << endl;

            Vector3d micro;
            micro << 1e-8, 0.0, 0.0;
            Quaterniond micro_q;
            micro_q.w() = 1;
            micro_q.vec() = micro / 2;
            Vector3d residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(6, length - 6) = residule_theta / 1e-8;

            micro << 0.0, 1e-8, 0.0;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(6, length - 5) = residule_theta / 1e-8;

            micro << 0.0, 0.0, 1e-8;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(6, length - 4) = residule_theta / 1e-8;

            //Pcb
            Jacobians[i].block<3, 3>(0, length - 3) = Rbc * frames[i].R.transpose() * (frames[i + 1].R - frames[i].R); 

            Jacobians[i] = sqrt_info * Jacobians[i];

            cout << Jacobians[i].block<15, 18>(0, i * 9) << endl;
            cout << Jacobians[i].block<15, 6>(0, length - 6) << endl;

        }

        MatrixXd H(length, length);
        H.setZero();
        VectorXd b(15);
        b.setZero();

        for(int i = 0;i < frames.size() - 1;i++)
        {
            H += Jacobians[i].transpose() * Jacobians[i];
            b += -Jacobians[i].transpose() * residuals[i];
        }

        if(b.transpose() * b < 0.00001)
            break;

        VectorXd delta = H.inverse() * b;

        for(int i = 0;i < length - 6;i = i + 9)
        {
            v[i] += Vector3d(delta(i), delta(i + 1), delta(i + 2));
            ba[i] += Vector3d(delta(i + 3), delta(i + 4), delta(i + 5));
            bg[i] += Vector3d(delta(i + 6), delta(i + 7), delta(i + 8));
        }

        cout << "v: " << v[0] << endl;
        cout << "ba: " << ba[0] << endl;
        cout << "bg: " << bg[0] << endl;
        cout << "pcb: " << Pcb << endl;

        Rbc = Rbc * Theta2Q(Vector3d(delta(length - 6), delta(length - 5), delta(length - 4))).toRotationMatrix();
        Pcb = Pcb + Vector3d(delta(length - 3), delta(length - 2), delta(length - 1));

        cout << "Optimizing Pcb = "<< Pcb << endl;
    }


    for(int i = 0;i < frames.size();i++)
    {
        frames[i].velocity = v[i];
        frames[i].imu_process.UpdatePreIntegration(ba[i], bg[i]);
    }

}

void Optimizer::OptimizeRbc()
{
    int length = (frames.size() - 1) * 3 + 3;

    double pre_residual = 1e20;
    while(true)
    {
        // Jacobians: |Jq_dbg1, Jq_dbg2, ... , Jq_dbgk, Jq_dRbc|
        //            |Jbg ...                                 |
        vector<MatrixXd> Jacobians(frames.size() - 1);
        vector<Matrix<double, 6, 1>> residuals(frames.size() - 1);
        
        //fix bg_0
        if(frames.size() != 0)
        {
            Jacobians[0].resize(6, length);
            Jacobians[0].setZero();

            //bg_j
            Jacobians[0].block<3, 3>(3, 0) = Matrix3d::Identity();

            //Rbc ???
            Quaterniond dq = frames[0].imu_process.pre_integration.delta_q;
            Vector3d micro;
            micro << 1e-5, 0.0, 0.0;
            Quaterniond micro_q;
            micro_q.w() = 1;
            micro_q.vec() = micro / 2;
            //cout << micro_q.vec() << endl;
            
            Vector3d residule_theta = 2 * ((dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[1].R.transpose() * frames[0].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (dq * Quaterniond(Rbc * frames[1].R.transpose() * frames[0].R * Rbc.transpose())).vec());
            Jacobians[0].block<3, 1>(0, length - 3) = residule_theta / 1e-5;

            micro << 0.0, 1e-5, 0.0;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[1].R.transpose() * frames[0].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (dq * Quaterniond(Rbc * frames[1].R.transpose() * frames[0].R * Rbc.transpose())).vec());
            Jacobians[0].block<3, 1>(0, length - 2) = residule_theta / 1e-5;

            micro << 0.0, 0.0, 1e-5;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[1].R.transpose() * frames[0].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (dq * Quaterniond(Rbc * frames[1].R.transpose() * frames[0].R * Rbc.transpose())).vec());
            Jacobians[0].block<3, 1>(0, length - 1) = residule_theta / 1e-5;

        }

        for(int i = 1;i < frames.size() - 1;i++)
        {
            Jacobians[i].resize(6, length);
            Jacobians[i].setZero();
            
            //bg_i
            Matrix3d dq_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
            Jacobians[i].block<3, 3>(0, (i - 1) * 3) = -dq_dbg;
            Jacobians[i].block<3, 3>(3, (i - 1) * 3) = -Matrix3d::Identity();
            
            //bg_j
            Jacobians[i].block<3, 3>(3, i * 3) = Matrix3d::Identity();
        
            //Rbc
            Quaterniond correct_dq = frames[i].imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg[i] - frames[i].imu_process.bg)); 
            
            Vector3d micro;
            micro << 1e-8, 0.0, 0.0;
            Quaterniond micro_q;
            micro_q.w() = 1;
            micro_q.vec() = micro / 2;
            Vector3d residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(0, length - 3) = residule_theta / 1e-8;

            micro << 0.0, 1e-8, 0.0;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(0, length - 2) = residule_theta / 1e-8;

            micro << 0.0, 0.0, 1e-8;
            micro_q.vec() = micro / 2;
            residule_theta = 2 * ((correct_dq * Quaterniond(Rbc * micro_q.toRotationMatrix() * frames[i + 1].R.transpose() * frames[i].R * micro_q.inverse().toRotationMatrix() * Rbc.transpose())).vec()
                                            - (correct_dq * Quaterniond(Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose())).vec());
            Jacobians[i].block<3, 1>(0, length - 1) = residule_theta / 1e-8;

        }

        for(int i = 0;i < frames.size() - 1;i++)
        {
            residuals[i].setZero();

            Matrix3d dq_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
            Quaterniond correct_q = frames[i].imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg[i] - frames[i].imu_process.bg));
            Matrix3d delta_R = correct_q * Rbc * frames[i + 1].R.transpose() * frames[i].R * Rbc.transpose();
            residuals[i].block<3, 1>(0, 0) = 2 * Quaterniond(delta_R).vec();
            residuals[i].block<3, 1>(3, 0) = bg[i + 1] - bg[i]; 
            // cout << bg[i].transpose() << "*****************************" << endl;
            // cout << bg[i + 1].transpose() << "**************************" << endl;
            // cout << residuals[i].block<3, 1>(3, 0) << "************************" << endl;
        }

        MatrixXd H(length, length);
        H.setZero();
        VectorXd b(length);
        b.setZero();
        
        double curr_residual = 0;
        for(int i = 0;i < frames.size() - 1;i++)
        {
            Matrix<double, 6, 6> convariance;
            convariance.setZero();
            convariance.block<3, 3>(0, 0) = frames[i].imu_process.pre_integration.covariance.block<3, 3>(O_R, O_R);
            convariance.block<3, 3>(3, 3) = frames[i].imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_BG);
            //convariance.setIdentity();
            H += Jacobians[i].transpose() * convariance.inverse() * Jacobians[i];
            b += -Jacobians[i].transpose() * convariance.inverse() * residuals[i];
            
            //cout << "H = " << H << endl;

            //cout << "b = " << b << endl;

            curr_residual += residuals[i].transpose() * convariance.inverse() * residuals[i];
            //cout << "convariance = " << convariance << endl;
            //cout << "convariance.inverse() = " << convariance.inverse() << endl;
            //cout << endl;
        }
        cout << "curr_residual = " << curr_residual << endl;

        if(curr_residual > pre_residual)
            break;
        else
            pre_residual = curr_residual;
        
        VectorXd delta = H.inverse() * b;
        cout << delta.transpose() << endl;

        cout << "residuals[0] = " << residuals[0] << endl;
        cout << "residuals[1] = " << residuals[1] << endl;
        cout << "residuals[2] = " << residuals[2] << endl;

        for(int i = 1;i < frames.size();i++)
        {
            bg[i] += delta.segment<3>(3 * (i - 1));
            cout << "bg[" << i << "]:" << bg[i].transpose() << " ";
        }
        cout << endl;

        Rbc = Rbc * Theta2Q(delta.tail<3>());

        cout << "Rbc = " << Rbc << endl;
    }
    cout << "--------------------------------------------------------" << endl;

    for(int i = 1;i < frames.size();i++)
    {
        cout << "optimized bg = " << i << " " << bg[i].transpose() << endl;
        frames[i].imu_process.UpdatePreIntegration(ba[i], bg[i]);
    }

}

void Optimizer::Optimize_Ceres_before()
{
    double qbc[4], pcb[3];
    double bgs[WINDOW_SIZE + 1][3], bas[WINDOW_SIZE + 1][3], Vs[WINDOW_SIZE + 1][3];

    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();

    pcb[0] = Pcb(0);
    pcb[1] = Pcb(1);
    pcb[2] = Pcb(2);

    for(int i = 0;i < frames.size();i++)
    {
        Vs[i][0] = frames[i].velocity(0);
        Vs[i][1] = frames[i].velocity(1);
        Vs[i][2] = frames[i].velocity(2);

        bgs[i][0] = frames[i].imu_process.bg(0);
        bgs[i][1] = frames[i].imu_process.bg(1);
        bgs[i][2] = frames[i].imu_process.bg(2);

        bas[i][0] = frames[i].imu_process.ba(0);
        bas[i][1] = frames[i].imu_process.ba(1);
        bas[i][2] = frames[i].imu_process.ba(2);
    }

    ceres::Problem problem;

    for(int i = 0;i < frames.size() - 1;i++)
    {
        //@<residuals, qbc, pcb, v_curr, ba_curr, bg_curr, v_next, ba_next, bg_next>
        ceres::CostFunction * cost_function = new ceres::NumericDiffCostFunction<CostFunctor, ceres::CENTRAL, 15, 4, 3, 3, 3, 3, 3, 3, 3>(new CostFunctor(frames[i], frames[i + 1], g));
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), qbc, pcb, Vs[i], bas[i], bgs[i], Vs[i + 1], bas[i + 1], bgs[i + 1]);
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();
    Pcb << pcb[0], pcb[1], pcb[2];

    cout << Rbc << endl;

    for(int i = 0;i < frames.size();i++)
    {
        cout << "Before Optimize bas[" << i << "]:" << frames[i].imu_process.ba.transpose() << endl;
        cout << "Before Optimize bgs[" << i << "]:" << frames[i].imu_process.bg.transpose() << endl;
        
        frames[i].velocity << Vs[i][0], Vs[i][1], Vs[i][2];
        //cout << "optimized bg = " << i << " " << bg[i].transpose() << endl;
        frames[i].imu_process.UpdatePreIntegration(Vector3d(bas[i][0], bas[i][1], bas[i][2]), Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));

        cout << "bas[" << i << "]: " << bas[i][0] << bas[i][1] << bas[i][2] << endl;
        cout << "bgs[" << i << "]: " << bgs[i][0] << bgs[i][1] << bgs[i][2] << endl; 
    }

    for(int i = 0;i <frames.size() - 1;i++)
    {
        cout << "Vs[" << i << "]: " << Vs[i][0] << Vs[i][1] << Vs[i][2] << endl;
        Vector3d v = (frames[i + 1].P - frames[i].P) / (frames[i + 1].timestamp - frames[i].timestamp);
        cout << "camera_v[" << i << "]: " << v.transpose() << endl;
    }    
}

void Optimizer::Optimize_Ceres(int flag)
{
    double qbc[4], pcb[3];

    int COUNT = frames.size();
    cout << frames.size() << endl;
    double bgs[WINDOW_SIZE + 1][3], bas[WINDOW_SIZE + 1][3], Vs[WINDOW_SIZE + 1][3];

    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();

    pcb[0] = Pcb(0);
    pcb[1] = Pcb(1);
    pcb[2] = Pcb(2);

    for(int i = 0;i < frames.size();i++)
    {
        Vs[i][0] = frames[i].velocity(0);
        Vs[i][1] = frames[i].velocity(1);
        Vs[i][2] = frames[i].velocity(2);

        bgs[i][0] = frames[i].imu_process.bg(0);
        bgs[i][1] = frames[i].imu_process.bg(1);
        bgs[i][2] = frames[i].imu_process.bg(2);

        bas[i][0] = frames[i].imu_process.ba(0);
        bas[i][1] = frames[i].imu_process.ba(1);
        bas[i][2] = frames[i].imu_process.ba(2);
    }

    // for(int i = 0;i < frames.size() - 1;i++)
    // {
    //     double dt = frames[i + 1].timestamp - frames[i].timestamp;
    //     Vector3d residual_p = Rbc * frames[i].R.transpose() * (frames[i + 1].P + frames[i + 1].R * Pcb - frames[i].P - frames[i].R * Pcb + 0.5 * g * dt * dt - frames[i].velocity * dt) - frames[i].imu_process.pre_integration.delta_p;
    //     Vector3d residual_v = Rbc * frames[i].R.transpose() * (frames[i + 1].velocity + g * dt - frames[i].velocity) - frames[i].imu_process.pre_integration.delta_v;
    //     Vector3d residual_q = 2 * (frames[i].imu_process.pre_integration.delta_q * Quaterniond(Rbc * frames[i + 1].R.transpose()) * Quaterniond(frames[i].R * Rbc.transpose())).vec();
    //     Vector3d residual_ba = frames[i + 1].imu_process.ba - frames[i].imu_process.ba;
    //     Vector3d residual_bg = frames[i + 1].imu_process.bg - frames[i].imu_process.bg;

    //     cout << "Optimize_Ceres() : residual_p = " << residual_p.transpose() << endl;
    //     cout << "                   residual_v = " << residual_v.transpose() << endl;
    //     cout << "                   residual_q = " << residual_q.transpose() << endl;
    //     cout << "                   residual_ba = " << residual_ba.transpose() << endl;
    //     cout << "                   residual_bg = " << residual_bg.transpose() << endl;

    //     MatrixXd cov = frames[i].imu_process.pre_integration.covariance;
    //     Matrix<double, 15, 15> sqrt_info = LLT<Matrix<double, 15, 15>>(cov.inverse()).matrixL().transpose();
    //     //cout << sqrt_info << endl;
    // }

    ceres::Problem problem;
    
    ceres::LocalParameterization *local_parameterization = new RotationLocalParameterization();
    problem.AddParameterBlock(qbc, 4, local_parameterization);
    //problem.SetParameterBlockConstant(qbc);

    problem.AddParameterBlock(pcb, 3);
    problem.SetParameterBlockConstant(pcb);

    if(flag != 1)
    {
        problem.AddParameterBlock(Vs[0], 3);
        problem.AddParameterBlock(bas[0], 3);
        problem.AddParameterBlock(bgs[0], 3);
        problem.SetParameterBlockConstant(Vs[0]);
        problem.SetParameterBlockConstant(bas[0]);
        problem.SetParameterBlockConstant(bgs[0]);      
    }

    for(int i = 0;i < frames.size() - 1;i++)
    {
        //@<residuals, qbc, pcb, v_curr, ba_curr, bg_curr, v_next, ba_next, bg_next>
        ceres::CostFunction * cost_function = new ceres::NumericDiffCostFunction<CostFunctor, ceres::CENTRAL, 15, 4, 3, 3, 3, 3, 3, 3, 3>(new CostFunctor(frames[i], frames[i + 1], g));
        problem.AddResidualBlock(cost_function, NULL, qbc, pcb, Vs[i], bas[i], bgs[i], Vs[i + 1], bas[i + 1], bgs[i + 1]);
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    // cout << summary.FullReport();
    // cout << summary.num_jacobian_evaluations << endl;

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();
    Pcb << pcb[0], pcb[1], pcb[2];

    cout << Rbc << endl;

    for(int i = 0;i < frames.size();i++)
    {
        cout << "Before Optimize bas[" << i << "]:" << frames[i].imu_process.ba.transpose() << endl;
        cout << "Before Optimize bgs[" << i << "]:" << frames[i].imu_process.bg.transpose() << endl;
        
        frames[i].velocity << Vs[i][0], Vs[i][1], Vs[i][2];
        frames[i].imu_process.UpdatePreIntegration(Vector3d(bas[i][0], bas[i][1], bas[i][2]), Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));

        Matrix3d dp_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);
        Matrix3d dp_dba = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);
        
        Matrix3d dq_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);
        
        Matrix3d dv_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);
        Matrix3d dv_dba = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);

        Vector3d ba_i, bg_i;
        ba_i << bas[i][0], bas[i][1], bas[i][2];
        bg_i << bgs[i][0], bgs[i][1], bgs[i][2];

        frames[i].imu_process.pre_integration.delta_p = frames[i].imu_process.pre_integration.delta_p + dp_dba * (ba_i - frames[i].imu_process.ba) + dp_dbg * (bg_i - frames[i].imu_process.bg); 
        frames[i].imu_process.pre_integration.delta_q = frames[i].imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (bg_i - frames[i].imu_process.bg)); 
        frames[i].imu_process.pre_integration.delta_v = frames[1].imu_process.pre_integration.delta_v + dv_dba * (ba_i - frames[i].imu_process.ba) + dv_dbg * (bg_i - frames[i].imu_process.bg);

        frames[i].imu_process.ba = ba_i;
        frames[i].imu_process.bg = bg_i;

        cout << "bas[" << i << "]: " << bas[i][0] << bas[i][1] << bas[i][2] << endl;
        cout << "bgs[" << i << "]: " << bgs[i][0] << bgs[i][1] << bgs[i][2] << endl; 
    }

    for(int i = 0;i <frames.size() - 1;i++)
    {
        cout << "Vs[" << i << "]: " << Vs[i][0] << Vs[i][1] << Vs[i][2] << endl;
        Vector3d v = (frames[i + 1].P - frames[i].P) / (frames[i + 1].timestamp - frames[i].timestamp);
        cout << "camera_v[" << i << "]: " << v.transpose() << endl;
    }    

    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d residual_p = Rbc * frames[i].R.transpose() * (frames[i + 1].P + frames[i + 1].R * Pcb - frames[i].P - frames[i].R * Pcb + 0.5 * g * dt * dt - frames[i].velocity * dt) - frames[i].imu_process.pre_integration.delta_p;
        Vector3d residual_v = Rbc * frames[i].R.transpose() * (frames[i + 1].velocity + g * dt - frames[i].velocity) - frames[i].imu_process.pre_integration.delta_v;
        Vector3d residual_q = 2 * (frames[i].imu_process.pre_integration.delta_q * Quaterniond(Rbc * frames[i + 1].R.transpose()) * Quaterniond(frames[i].R * Rbc.transpose())).vec();
        Vector3d residual_ba = frames[i + 1].imu_process.ba - frames[i].imu_process.ba;
        Vector3d residual_bg = frames[i + 1].imu_process.bg - frames[i].imu_process.bg;

        //cout << "residual_v " << i << " = "<< residual_v.transpose() << endl;

        MatrixXd cov = frames[i].imu_process.pre_integration.covariance;
        Matrix<double, 15, 15> sqrt_info = LLT<Matrix<double, 15, 15>>(cov.inverse()).matrixL().transpose();
        //cout << "covariance " << i << " = " << cov.block<1, 15>(0, 0) << endl;
        //cout <<" covariance.inverse() " << i << " = " << cov.inverse().block<1, 15>(0, 0) << endl;

        //cout << "sqrt_info" << i << " = " << sqrt_info.block<1, 15>(0, 0) << endl;
    }
}

void Optimizer::Optimize_Ceres_Test()
{
    double qbc[4], pcb[3];
    double bgs[WINDOW_SIZE + 1][3], bas[WINDOW_SIZE + 1][3], Vs[WINDOW_SIZE + 1][3];

    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();

    pcb[0] = Pcb(0);
    pcb[1] = Pcb(1);
    pcb[2] = Pcb(2);

    for(int i = 0;i < frames.size();i++)
    {
        Vs[i][0] = frames[i].velocity(0);
        Vs[i][1] = frames[i].velocity(1);
        Vs[i][2] = frames[i].velocity(2);

        bgs[i][0] = frames[i].imu_process.bg(0);
        bgs[i][1] = frames[i].imu_process.bg(1);
        bgs[i][2] = frames[i].imu_process.bg(2);

        bas[i][0] = frames[i].imu_process.ba(0);
        bas[i][1] = frames[i].imu_process.ba(1);
        bas[i][2] = frames[i].imu_process.ba(2);
    }

    ceres::Problem problem;
    
    ceres::LocalParameterization *local_parameterization = new RotationLocalParameterization();
    problem.AddParameterBlock(qbc, 4, local_parameterization);
    problem.SetParameterBlockConstant(qbc);

    problem.AddParameterBlock(pcb, 3);
    problem.SetParameterBlockConstant(pcb);

    problem.AddParameterBlock(Vs[0], 3);
    problem.AddParameterBlock(bas[0], 3);
    problem.AddParameterBlock(bgs[0], 3);
    problem.SetParameterBlockConstant(Vs[0]);
    problem.SetParameterBlockConstant(bas[0]);
    problem.SetParameterBlockConstant(bgs[0]);

    

    for(int i = 0;i < frames.size() - 1;i++)
    {
        //@<residuals, qbc, pcb, v_curr, ba_curr, bg_curr, v_next>
        ceres::CostFunction * cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Test, ceres::CENTRAL, 9, 4, 3, 3, 3, 3, 3>(new CostFunctor_Test(frames[i], frames[i + 1], g));
        problem.AddResidualBlock(cost_function, NULL, qbc, pcb, Vs[i], bas[i], bgs[i], Vs[i + 1]);
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();
    Pcb << pcb[0], pcb[1], pcb[2];

    cout << Rbc << endl;

    for(int i = 0;i < frames.size();i++)
    {
        cout << "Before Optimize bas[" << i << "]:" << frames[i].imu_process.ba.transpose() << endl;
        cout << "Before Optimize bgs[" << i << "]:" << frames[i].imu_process.bg.transpose() << endl;
        
        frames[i].velocity << Vs[i][0], Vs[i][1], Vs[i][2];

        Matrix3d dp_dbg = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);
        Matrix3d dp_dba = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);

        Vector3d ba_i, bg_i;
        ba_i << bas[i][0], bas[i][1], bas[i][2];
        bg_i << bgs[i][0], bgs[i][1], bgs[i][2];
        
        frames[i].imu_process.UpdatePreIntegration(Vector3d(bas[i][0], bas[i][1], bas[i][2]), Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));
        cout << "delta_p " << frames[i].imu_process.pre_integration.delta_p << endl;

        cout << "dp_dba[" << i << "]= " << endl;
        cout << dp_dba << endl;

        // frames[i].imu_process.pre_integration.delta_p = frames[i].imu_process.pre_integration.delta_p + dp_dba * (ba_i - frames[i].imu_process.ba) + dp_dbg * (bg_i - frames[i].imu_process.bg); 

        // frames[i].imu_process.ba = ba_i;
        // frames[i].imu_process.bg = bg_i;

        cout << "bas[" << i << "]: " << bas[i][0] << bas[i][1] << bas[i][2] << endl;
        cout << "bgs[" << i << "]: " << bgs[i][0] << bgs[i][1] << bgs[i][2] << endl; 
    }

    for(int i = 0;i <frames.size() - 1;i++)
    {
        cout << "Vs[" << i << "]: " << Vs[i][0] << Vs[i][1] << Vs[i][2] << endl;
        Vector3d v = (frames[i + 1].P - frames[i].P) / (frames[i + 1].timestamp - frames[i].timestamp);
        cout << "camera_v[" << i << "]: " << v.transpose() << endl;
    }    
}


void Optimizer::Optimize_NoVelocity_Ceres()
{
    double qbc[4], pcb[3];
    double bgs[WINDOW_SIZE + 1][3], bas[WINDOW_SIZE + 1][3], Vs[WINDOW_SIZE + 1][3];

    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();

    pcb[0] = Pcb(0);
    pcb[1] = Pcb(1);
    pcb[2] = Pcb(2);

    for(int i = 0;i < frames.size();i++)
    {
        Vs[i][0] = frames[i].velocity(0);
        Vs[i][1] = frames[i].velocity(1);
        Vs[i][2] = frames[i].velocity(2);

        bgs[i][0] = frames[i].imu_process.bg(0);
        bgs[i][1] = frames[i].imu_process.bg(1);
        bgs[i][2] = frames[i].imu_process.bg(2);

        bas[i][0] = frames[i].imu_process.ba(0);
        bas[i][1] = frames[i].imu_process.ba(1);
        bas[i][2] = frames[i].imu_process.ba(2);
    }

    ceres::Problem problem;

    for(int i = 0;i < frames.size() - 1;i++)
    {
        //@<residuals, qbc, pcb, v_curr, ba_curr, bg_curr, v_next, ba_next, bg_next>
        ceres::CostFunction * cost_function = new ceres::NumericDiffCostFunction<CostFunctor_NoVelocity, ceres::CENTRAL, 15, 4, 3, 3, 3, 3, 3>(new CostFunctor_NoVelocity(frames[i], frames[i + 1], g));
        problem.AddResidualBlock(cost_function, new ceres::CauchyLoss(0.5), qbc, pcb, bas[i], bgs[i], bas[i + 1], bgs[i + 1]);
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();
    Pcb << pcb[0], pcb[1], pcb[2];

    cout << Rbc << endl;

    for(int i = 0;i < frames.size();i++)
    {
        cout << "Before Optimize bas[" << i << "]:" << frames[i].imu_process.ba.transpose() << endl;
        cout << "Before Optimize bgs[" << i << "]:" << frames[i].imu_process.bg.transpose() << endl;
        
        frames[i].velocity << Vs[i][0], Vs[i][1], Vs[i][2];
        //cout << "optimized bg = " << i << " " << bg[i].transpose() << endl;
        frames[i].imu_process.UpdatePreIntegration(Vector3d(bas[i][0], bas[i][1], bas[i][2]), Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));

        cout << "bas[" << i << "]: " << bas[i][0] << bas[i][1] << bas[i][2] << endl;
        cout << "bgs[" << i << "]: " << bgs[i][0] << bgs[i][1] << bgs[i][2] << endl; 
    }

    for(int i = 0;i <frames.size() - 1;i++)
    {
        cout << "Vs[" << i << "]: " << Vs[i][0] << Vs[i][1] << Vs[i][2] << endl;
        Vector3d v = (frames[i + 1].P - frames[i].P) / (frames[i + 1].timestamp - frames[i].timestamp);
        cout << "camera_v[" << i << "]: " << v.transpose() << endl;
    }    
}

void Optimizer::OptimizeRbc_Ceres()
{

    file_info.open("/home/likaiyue/VI_Calibration/info.txt", ofstream::out | ofstream::app);   

    ofstream fixed_first_Rbc;
    fixed_first_Rbc.open("/home/likaiyue/VI_Calibration/CostFunctor_Rbc/unfixed_first_Rbc.txt", ofstream::out | ofstream::app);
    
    double qbc[4];
    double bgs[WINDOW_SIZE + 1][3];

    // WINDOW_SIZE + 1 = frames.size()
    if(frames.size() < WINDOW_SIZE + 1)
    {
        printf("the frames in Window is not enough!\n");
        exit(-1);
    }

    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();
    for(int i = 0;i < WINDOW_SIZE + 1;i++)
    {   
        bgs[i][0] = frames[i].imu_process.bg(0);
        bgs[i][1] = frames[i].imu_process.bg(1);
        bgs[i][2] = frames[i].imu_process.bg(2);
        cout << "before= "<< bgs[i][0] << bgs[i][1] << bgs[i][2] << endl; 
    }
    
    // for(int i = 0;i < frames.size() - 1;i++)
    // {
    //     Quaterniond qbc_temp(qbc[0], qbc[1], qbc[2], qbc[3]);
    //     Vector3d residual = 2 * (frames[i].imu_process.pre_integration.delta_q * qbc_temp * Quaterniond(frames[i + 1].R.transpose()) * Quaterniond(frames[i].R) * qbc_temp.inverse()).vec();
    //     cout << "residual" << i << ":" << residual.transpose() << endl;
    // }
    
    ceres::Problem problem;

    ceres::LocalParameterization *local_parameterization = new RotationLocalParameterization();
    problem.AddParameterBlock(qbc, 4, local_parameterization);

    ceres::CostFunction *cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Rbc1, ceres::CENTRAL, 6, 4, 3>(new CostFunctor_Rbc1(frames[0].imu_process, frames[0].R, frames[1].R));
    problem.AddResidualBlock(cost_function, NULL, qbc, bgs[1]);

    for(int i = 1;i < frames.size() - 1;i++)
    {
        ceres::CostFunction *cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Rbc, ceres::CENTRAL, 6, 4, 3, 3>(new CostFunctor_Rbc(frames[i].imu_process, frames[i].R, frames[i + 1].R));
        problem.AddResidualBlock(cost_function, NULL, qbc, bgs[i], bgs[i + 1]);
    }
    cout << "+++" << endl;

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();

    for(int i = 0;i < frames.size();i++)
    {
        //cout << "after= " << bgs[i][0] << bgs[i][1] << bgs[i][2] << endl;
        frames[i].imu_process.UpdatePreIntegration(ba[i], Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));
    }

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Quaterniond qbc_temp(qbc[0], qbc[1], qbc[2], qbc[3]);
        Vector3d residual = 2 * (frames[i].imu_process.pre_integration.delta_q * qbc_temp * Quaterniond(frames[i + 1].R.transpose()) * Quaterniond(frames[i].R) * qbc_temp.inverse()).vec();
        //AngleAxisd aa(residual.norm(), residual.normalized());
        Quaterniond delat_q_cout = frames[i].imu_process.pre_integration.delta_q * qbc_temp * Quaterniond(frames[i + 1].R.transpose()) * Quaterniond(frames[i].R) * qbc_temp.inverse();
       
        cout << "eularAngles_residual [" << i << "]= " <<  delat_q_cout.toRotationMatrix().eulerAngles(2, 1, 0) << endl;

        file_info << "residual" << i << ":" << residual.transpose() << endl;
        cout << "residual" << i << ":" << residual.transpose() << endl;
        cout << "angleAxis_residual = " << i << ":" << residual.transpose() << endl;
        cout << "angleAxis_residual_norm() = " << residual.norm() << endl;
        Matrix<double, 6, 6> info;
        info.setIdentity();
        //info.block<3, 3>(0, 0) = Matrix3d::Identity() * 1e-20;
        info.block<3, 3>(0, 0) = frames[i].imu_process.pre_integration.covariance.block<3, 3>(O_R, O_R);
        info.block<3, 3>(3, 3) = frames[i].imu_process.pre_integration.covariance.block<3, 3>(O_BG, O_BG);
        Matrix<double, 6, 6> sqrt_info = LLT<Matrix<double, 6, 6>>(info.inverse()).matrixL().transpose();
        //file_info << "sqrt_info = " << sqrt_info << endl;
    }

    cout << "BriefReport()= " << summary.BriefReport() << endl;

    cout << "Rbc=" << endl;
    cout << Rbc << endl;

    file_info << "Rbc=" << endl;
    file_info << Rbc << endl;

    fixed_first_Rbc << "Rbc=" << endl;
    fixed_first_Rbc << Rbc << endl;
    file_info << "-------------------------------------------------------------------------------------------" << endl;

    fixed_first_Rbc.clear();
    file_info.close();

}


void Optimizer::OptimizeRbc_Ceres_Test()
{
    double qbc[4];
    double bgs[WINDOW_SIZE + 1][3];

    // WINDOW_SIZE + 1 = frames.size()
    if(frames.size() < WINDOW_SIZE + 1)
    {
        printf("the frames in Window is not enough!\n");
        exit(-1);
    }
    
    Quaterniond q(Rbc);
    qbc[0] = q.w();
    qbc[1] = q.x();
    qbc[2] = q.y();
    qbc[3] = q.z();
    for(int i = 0;i < WINDOW_SIZE + 1;i++)
    {   
        bgs[i][0] = bg[i].x();
        bgs[i][1] = bg[i].y();
        bgs[i][2] = bg[i].z();
    }

    ceres::Problem problem;

    // ceres::CostFunction *cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Rbc1, ceres::CENTRAL, 6, 4, 3>(new CostFunctor_Rbc1(frames[0].imu_process, frames[0].R, frames[1].R));
    // problem.AddResidualBlock(cost_function, NULL, qbc, bgs[1]);

    // for(int i = 1;i < frames.size() - 1;i++)
    // {
    //     ceres::CostFunction *cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Rbc, ceres::CENTRAL, 6, 4, 3, 3>(new CostFunctor_Rbc(frames[i].imu_process, frames[i].R, frames[i + 1].R));
    //     problem.AddResidualBlock(cost_function, NULL, qbc, bgs[i], bgs[i + 1]);
    // }
    // cout << "+++" << endl;

    Quaterniond qbc_test(Rbc);
    for(int i = 0;i < frames.size() - 1;i++)
    {
        ceres::CostFunction *cost_function = new ceres::NumericDiffCostFunction<CostFunctor_Rbc_Test, ceres::CENTRAL, 3, 3>(new CostFunctor_Rbc_Test(frames[i].imu_process, frames[i].R, frames[1 + 1].R, qbc_test));
        problem.AddResidualBlock(cost_function, NULL, bgs[i]);
    }

    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    Rbc = Quaterniond(qbc[0], qbc[1], qbc[2], qbc[3]).toRotationMatrix();

    for(int i = 0;i < frames.size();i++)
    {
        frames[i].imu_process.UpdatePreIntegration(ba[i], Vector3d(bgs[i][0], bgs[i][1], bgs[i][2]));
    }

    cout << Rbc << endl;

}

Matrix<double, 15, 1> Optimizer::Evaluate(Frame frame_curr, Frame frame_next, Vector3d V_i, Vector3d Ba_i, Vector3d Bg_i, Vector3d V_j, Vector3d Ba_j, Vector3d Bg_j)
{
    Matrix<double, 15, 1> residual;
    residual.setZero();

    double sum_dt = frame_next.timestamp - frame_curr.timestamp;

    Eigen::Matrix3d dp_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BA);
    Eigen::Matrix3d dp_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_P, O_BG);

    Eigen::Matrix3d dq_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_R, O_BG);

    Eigen::Matrix3d dv_dba = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BA);
    Eigen::Matrix3d dv_dbg = frame_curr.imu_process.pre_integration.jacobian.block<3, 3>(O_V, O_BG);

    Quaterniond Q_curr(frame_curr.R * Rbc.transpose());
    Quaterniond Q_next(frame_next.R * Rbc.transpose());
    
    Quaterniond corrected_delta_q = frame_curr.imu_process.pre_integration.delta_q * Theta2Q(dq_dbg * (Bg_i - frame_curr.imu_process.bg));
    Vector3d corrected_delta_p = frame_curr.imu_process.pre_integration.delta_p + dp_dba * (Ba_i - frame_curr.imu_process.ba) + dp_dbg * (Bg_i - frame_curr.imu_process.bg);
    Vector3d corrected_delta_v = frame_curr.imu_process.pre_integration.delta_v + dv_dba * (Ba_i - frame_curr.imu_process.ba) + dv_dbg * (Bg_i - frame_curr.imu_process.bg);

    residual.block<3, 1>(0, 0) = Rbc * frame_curr.R.transpose() * (frame_next.P + frame_next.R * Pcb - frame_curr.P - frame_curr.R * Pcb + 0.5 * g * sum_dt * sum_dt - V_i * sum_dt) - corrected_delta_p;
    residual.block<3, 1>(3, 0) = Rbc * frame_curr.R.transpose() * (V_j + g * sum_dt - V_i) - corrected_delta_v;
    residual.block<3, 1>(6, 0) = 2 * (corrected_delta_q * Q_next.inverse() * Q_curr).vec();
    residual.block<3, 1>(9, 0) = Ba_j - Ba_i;
    residual.block<3, 1>(12, 0) = Bg_j - Bg_i;

    return residual;

}


