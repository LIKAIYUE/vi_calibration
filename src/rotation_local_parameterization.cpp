#include"rotation_local_parameterization.h"
#include<Eigen/Dense>
#include<Utility.h>

using namespace Eigen;

bool RotationLocalParameterization::Plus(const double *x, const double *delta, double *x_plus_delta) const
{
    Quaterniond qbc(x[0], x[1], x[2], x[3]);
    Vector3d theta(delta[0], delta[1], delta[2]);

    Quaterniond _qbc = qbc * Theta2Q(theta);
    qbc = _qbc.normalized();

    x_plus_delta[0] = qbc.w();
    x_plus_delta[1] = qbc.x();
    x_plus_delta[2] = qbc.y();
    x_plus_delta[3] = qbc.z();

    return true;
}

bool RotationLocalParameterization::ComputeJacobian(const double *x, double *jacobian) const
{
    Map<Matrix<double, 4, 3, Eigen::RowMajor>> jaco(jacobian);
    Quaterniond qbc(x[0], x[1], x[2], x[3]);

    Matrix<double, 4, 3> j2;
    j2.topRows<1>().setZero();
    j2.block<3, 3>(1, 0) = 0.5 * Matrix3d::Identity();

    jaco = Qleft(qbc) * j2;

    return true;
}