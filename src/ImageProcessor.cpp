#include<stdio.h>
#include<stdlib.h>
#include"ImageProcessor.h"
#include"parameter.h"

void ImageProcessor::LoadFrames(string path)
{

    Vector3d front_extri_param_r(camera_front_rx, camera_front_ry, camera_front_rz);
    Vector3d front_extri_param_t(camera_front_tx, camera_front_ty, camera_front_tz);

    AngleAxisd front_extri_Axisd(front_extri_param_r.norm(), front_extri_param_r.normalized());
    Matrix3d front_extri_R = front_extri_Axisd.toRotationMatrix();

    FILE* frames_file = nullptr;
    frames_file = fopen(path.c_str(), "rt");

    if(!frames_file)
    {
        printf("can't load frames_info file %s\n", path.c_str());
        exit(-1);
    }

    while(!feof(frames_file))
    {
        double timestamp, rx, ry, rz, px, py, pz;
        fscanf(frames_file, "%lf %lf %lf %lf %lf %lf %lf", &timestamp, &rx, &ry, &rz, &px, &py, &pz);

        Frame frame;
        frame.timestamp = timestamp / time_stamp_to_sec + 0.08903700413589564;
        
        Vector3d r3, t3;
        r3 << rx, ry, rz;
        AngleAxisd aa(r3.norm(), r3.normalized());
        frame.R = aa.toRotationMatrix().transpose();

        AngleAxisd aa1(3, Vector3d(2, 2, 0).normalized());
        Matrix3d R_cc(aa1);

        frame.R = frame.R;
        
        t3 << px, py, pz;
        t3 = -aa.toRotationMatrix().transpose() * t3;
        frame.P = t3;

        frames.push_back(frame);

    }

    // for(int i = 0;i < frames.size();i++)
    // {
    //     if(i == 0)
    //         frames[i].velocity = (frames[i + 1].P - frames[i].P) / (frames[i + 1].timestamp - frames[i].timestamp);
    //     else
    //     {
    //         if(i == frames.size() - 1)
    //             frames[i].velocity = (frames[i].P - frames[i - 1].P) / (frames[i].timestamp - frames[i - 1].timestamp);
    //         else
    //             frames[i].velocity = (frames[i + 1].P - frames[i - 1].P) / (frames[i + 1].timestamp - frames[i - 1].timestamp);
    //     }
    // }

    // Matrix3d R0;
    // Vector3d P0;
    // if(frames.size() != 0)
    // {
    //     R0 = frames[0].R;
    //     P0 = frames[0].P;
    // }
    // for(int i = 0;i < frames.size();i++)
    // {
    //     frames[i].R = R0.transpose() * frames[i].R;
    //     frames[i].P = R0.transpose() * (frames[i + 1].P - P0);
    // }

    fclose(frames_file);
}

void ImageProcessor::LoadGroundTruth(string path)
{
    FILE* ground_truth_file = nullptr;
    ground_truth_file = fopen(path.c_str(), "r");

    if(!ground_truth_file)
    {
        exit(-1);
        printf("cann't load the groundtruth file %s\n", path.c_str());
    }

    char format[1000];
    fscanf(ground_truth_file, "%[^\n]\n", format);
    printf("%s\n", format);

    Vector3d Pbc;
    Pbc << 0.2, 0.3, 0.1;
    int interval = 0;
    while(!feof(ground_truth_file))
    {
        //#timestamp, p_RS_R_x [m], p_RS_R_y [m], p_RS_R_z [m], q_RS_w [], q_RS_x [], q_RS_y [], q_RS_z [], 
        //v_RS_R_x [m s^-1], v_RS_R_y [m s^-1], v_RS_R_z [m s^-1], 
        //b_w_RS_S_x [rad s^-1], b_w_RS_S_y [rad s^-1], b_w_RS_S_z [rad s^-1], b_a_RS_S_x [m s^-2], b_a_RS_S_y [m s^-2], b_a_RS_S_z [m s^-2]
        Frame frame;
        //1 + 7 + 3 + 6
        double timestamp, p_x, p_y, p_z, q_w, q_x, q_y, q_z, v_x, v_y, v_z, bg_x, bg_y, bg_z, ba_x, ba_y, ba_z;
        fscanf(ground_truth_file, "%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf", &timestamp, &p_x, &p_y, &p_z, &q_w, &q_x, &q_y, &q_z, &v_x, &v_y, &v_z, &bg_x, &bg_y, &bg_z, &ba_x, &ba_y, &ba_z);
        // printf("%lf", p_x);

        frame.timestamp = timestamp / time_stamp_to_sec;

        AngleAxisd aa(2, Vector3d(1, 0, 0));
        //Quaterniond qbc_test(1, 0.01, 0.01, 0.01);
        //qbc_test.normalize();

        Matrix3d Rbc;
        Rbc << 0.0148655429818, -0.999880929698, 0.00414029679422,
               0.999557249008, 0.0149672133247, 0.025715529948,
               -0.0257744366974, 0.00375618835797, 0.999660727178;

        Quaterniond q(q_w, q_x, q_y, q_z);  
        frame.R = q.toRotationMatrix() * Rbc;

        Vector3d P;
        P << p_x, p_y, p_z;
        //P = P + frame.R * Pbc;
        frame.P = P;

        frame.velocity << v_x, v_y, v_z;
        frame.imu_process.ba << ba_x, ba_y, ba_z;
        frame.imu_process.bg << bg_x, bg_y, bg_z;
        //printf("%lf %lf %lf\n", frame.imu_process.ba.x(), frame.imu_process.ba.y(), frame.imu_process.ba.z());

        interval++;
        if(interval == 10)
        {
            interval = 0;
            frames.push_back(frame);
        }
        

        if(frames.size() > 5000)
            break;
    }

    fclose(ground_truth_file);
    
}