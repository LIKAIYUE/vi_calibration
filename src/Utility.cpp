#include"Utility.h"

Quaterniond Theta2Q(Vector3d deltaTheta)
{
    Quaterniond Q;
    Q.w() = 1.0;
    Q.x() = 0.5 * deltaTheta(0);
    Q.y() = 0.5 * deltaTheta(1);
    Q.z() = 0.5 * deltaTheta(2);

    return Q;
}

Matrix<double, 3, 3> skewSymmetric(Vector3d q)
{
    Matrix<double, 3, 3> ans;
    ans << 0.0, -q(2), q(1),
        q(2), 0.0, -q(0),
        -q(1), q(0), 0.0;
    return ans;
}

Matrix<double, 4, 4> Qleft(Quaterniond qq)
{
    Matrix<double, 4, 4> ans;
    ans(0, 0) = qq.w(), ans.template block<1, 3>(0, 1) = -qq.vec().transpose();
    ans.template block<3, 1>(1, 0) = qq.vec(), ans.template block<3, 3>(1, 1) = qq.w() * Matrix3d::Identity() + skewSymmetric(qq.vec());
    return ans;
}

Matrix<double, 4, 4> Qright(Quaterniond pp)
{
    Matrix<double, 4, 4> ans;
    ans(0, 0) = pp.w(), ans.template block<1, 3>(0, 1) = -pp.vec().transpose();
    ans.template block<3, 1>(1, 0) = pp.vec(), ans.template block<3, 3>(1, 1) = pp.w() * Matrix3d::Identity() - skewSymmetric(pp.vec());
    return ans;
}

