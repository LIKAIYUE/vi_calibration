#include<iostream>
#include<stdio.h>
#include<iomanip>

#include"ImageProcessor.h"
#include"Initializer.h"
#include"Optimizer.h"
#include"IMUBase.h"
#include"parameter.h"

#include<unistd.h>

#include<ros/ros.h>
#include<nav_msgs/Path.h>
#include<nav_msgs/Odometry.h>

using namespace std;

bool EUROC = true;

ros::Publisher path_publisher;
ros::Publisher odometry_publisher;

nav_msgs::Path path;

Matrix3d Rbc;
Vector3d Pbc;
Vector3d g;
double s;

const int MIN_FRAME_NUM = 20;
int frame_start = 0;

int num_for_init = 0;

vector<Frame> frames;


void publishPath(float x, float y, float z, float qw, float qx, float qy, float qz) {
	geometry_msgs::PoseStamped this_pose_stamped;
	this_pose_stamped.pose.position.x = x;
	this_pose_stamped.pose.position.y = y;
	this_pose_stamped.pose.position.z = z;

	this_pose_stamped.pose.orientation.x = qx;
	this_pose_stamped.pose.orientation.y = qy;
	this_pose_stamped.pose.orientation.z = qz;
	this_pose_stamped.pose.orientation.w = qw;

	this_pose_stamped.header.stamp = ros::Time::now();
	this_pose_stamped.header.frame_id = "map";

	path.header.stamp = ros::Time::now();
    path.header.frame_id = "map";

	path.poses.push_back(this_pose_stamped);
	path_publisher.publish(path);
}


void publishOdometry(float x, float y, float z, float qw, float qx, float qy, float qz, float vx, float vy, float vz)
{
    nav_msgs::Odometry odometry;

    odometry.header.stamp = ros::Time::now();
    odometry.header.frame_id = "world";
    
    odometry.pose.pose.position.x = x;
    odometry.pose.pose.position.y = y;
    odometry.pose.pose.position.z = z;
    
    odometry.pose.pose.orientation.x = qx;
    odometry.pose.pose.orientation.y = qy;
    odometry.pose.pose.orientation.z = qz;
    odometry.pose.pose.orientation.w = qw;

    odometry.twist.twist.linear.x = vx;
    odometry.twist.twist.linear.y = vx;
    odometry.twist.twist.linear.z = vz;

    odometry_publisher.publish(odometry);
}

IMUData Interpolation(double image_timestamp, IMUData pre_imu_data, IMUData imu_data)
{
    IMUData interpolation;
    double dt1 = image_timestamp - pre_imu_data.timestamp;
    double dt2 = imu_data.timestamp - image_timestamp; 
    double dt = dt1 + dt2;

    double ax, ay, az, wx, wy, wz;
    ax = pre_imu_data.acc.x() + dt1 / dt * (imu_data.acc.x() - pre_imu_data.acc.x());
    ay = pre_imu_data.acc.y() + dt1 / dt * (imu_data.acc.y() - pre_imu_data.acc.y());
    az = pre_imu_data.acc.z() + dt1 / dt * (imu_data.acc.z() - pre_imu_data.acc.z());
    wx = pre_imu_data.gyr.x() + dt1 / dt * (imu_data.gyr.x() - pre_imu_data.gyr.x());
    wy = pre_imu_data.gyr.y() + dt1 / dt * (imu_data.gyr.y() - pre_imu_data.gyr.y());;
    wz = pre_imu_data.gyr.z() + dt1 / dt * (imu_data.gyr.z() - pre_imu_data.gyr.z());;
    interpolation.timestamp = image_timestamp;
    interpolation.acc << ax, ay, az;
    interpolation.gyr << wx, wy, wz;
    return interpolation;
}

int SelectTheBestDataForInit(int _frame_start)
{   
    if(frames.size() < _frame_start + MIN_FRAME_NUM) { exit(-1); }
    
    vector<double> vars;

    Vector3d sum_g;
    for(int i = _frame_start + MIN_FRAME_NUM;i < frames.size();i++)
    {
        vector<double> v;
        for (int j = i - MIN_FRAME_NUM;j < i;j++)
        {
            double dt = frames[j + 1].timestamp - frames[j].timestamp;
            v.push_back(sqrt((frames[j + 1].P - frames[j].P).transpose() * (frames[j + 1].P - frames[j].P)) / dt);
        }

        double sum_v = 0;
        for(int j = 0;j < v.size();j++)
            sum_v += v[j];
        
        double aver_v;
        aver_v = sum_v / MIN_FRAME_NUM;
        
        double var = 0;
        for(int j = 0;j < v.size();j++)
            var += (v[j] - aver_v) * (v[j] - aver_v);
            
        var = sqrt(var / MIN_FRAME_NUM);
        vars.push_back(var);
    }

    double max = 0;
    int max_id; 
    for(int j = 0;j < vars.size();j++)
    {   
        if(max < vars[j])
        {
            max = vars[j];
            max_id = _frame_start + j;
        }
    }
    return max_id;
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "VI_frames_ros", ros::init_options::NoSigintHandler);
	ros::NodeHandle n;
	path_publisher = n.advertise<nav_msgs::Path>("VI_frames", 1, true); // -- set path publisher
    
    odometry_publisher = n.advertise<nav_msgs::Odometry>("odometry", 1, true);

    string data_dir = argv[1];
    ImageProcessor image_processor;
    
    if(EUROC)
    {
        string ground_truth_path = "/home/likaiyue/test/Euroc_Data/mav0/state_groundtruth_estimate0/data.csv";
        image_processor.LoadGroundTruth(ground_truth_path.c_str());
    }
    else
    {
        string image_path = data_dir + "/frames_info.txt";
        image_processor.LoadFrames(image_path);
    }
    
    frames = image_processor.frames;

    // if(EUROC)
    // {
    //     double the_timestamp_find_start = 1403636580163555584 / time_stamp_to_sec;

    //     for(;frame_start < frames.size();frame_start++)
    //     {
    //         cout << fixed << frames[frame_start].timestamp << endl;
    //         cout << fixed << the_timestamp_find_start << endl;
    //         cout << endl;
    //         if(frame_start > 50)
    //             exit(-1);
    //         if((frames[frame_start].timestamp - the_timestamp_find_start) < 0.1 && (frames[frame_start].timestamp - the_timestamp_find_start) > -0.1)
    //             break;
    //     }

    // }
            
    cout << " frame_start = " << frame_start << endl;

    for(int i = 0;i < frames.size();i++)
    {
        usleep(100);
        //cout << frames[i].P << endl;
        Quaterniond q(frames[i].R);
        //publishOdometry(frames[i].P.x(), frames[i].P.y(), frames[i].P.z(), q.w(), q.x(), q.y(), q.z(), frames[i].velocity.x(), frames[i].velocity.y(), frames[i].velocity.z());

        //publishPath(frames[i].P.x(), frames[i].P.y(), frames[i].P.z(), q.w(), q.x(), q.y(), q.z());
    
    }
    //exit(-1);

    string imu_euroc_path = "/home/likaiyue/test/Euroc_Data/mav0/imu0/data.csv";
    FILE* imu_euroc_file = nullptr;
    imu_euroc_file = fopen(imu_euroc_path.c_str(), "r");

    char format[7][500];
    fscanf(imu_euroc_file, "%[^,], %[^,], %[^,], %[^,], %[^,], %[^,], %[^\n] ", format[0], format[1], format[2], format[3], format[4], format[5], format[6]);
    
    // for(int i = 0;i < 7;i++)
    //     cout << format[i] << endl;
    // cout << endl;
    // char test_c[500];
    // fscanf(imu_euroc_file, "%[^\n]", test_c);
    // cout << test_c << endl;

    string imu_path = data_dir + "/imu0.txt";
    FILE* imu_file = nullptr;
    imu_file = fopen(imu_path.c_str(), "rt");

    if(!imu_file)
    {
        printf("can not load imu data file %s\n", imu_path.c_str());
        exit(-1);
    }

    IMUData imu_data;
    if(EUROC)
    {    
        double timestamp_euroc, ax_euroc, ay_euroc, az_euroc, wx_euroc, wy_euroc, wz_euroc;
        fscanf(imu_euroc_file, "%lf, %lf, %lf, %lf, %lf, %lf, %lf", &timestamp_euroc, &wx_euroc, &wy_euroc, &wz_euroc, &ax_euroc, &ay_euroc, &az_euroc);
        imu_data.timestamp = timestamp_euroc / time_stamp_to_sec;
        imu_data.acc << ax_euroc, ay_euroc, az_euroc;
        imu_data.gyr << wx_euroc, wy_euroc, wz_euroc;
    }
    else
    {
        //timestamp  ax ay az wx wy wz
        double timestamp, ax, ay, az, wx, wy, wz;
        fscanf(imu_file, "%lf %lf %lf %lf %lf %lf %lf", &timestamp, &wx, &wy, &wz, &ax, &ay, &az);
        imu_data.timestamp = timestamp / time_stamp_to_sec;
        imu_data.acc << ax, ay, az;
        imu_data.gyr << wx, wy, wz;
    }
    
    while(frame_start < frames.size() && frames[frame_start].timestamp < imu_data.timestamp)
        frame_start++;

    //frame_start = SelectTheBestDataForInit(frame_start);
    //cout << frame_start << endl;
    //cout << fixed << setprecision(19) <<frames[frame_start].timestamp << endl;
    
    IMUData pre_imu_data;
    while(true)
    {
        if(imu_data.timestamp <= frames[frame_start].timestamp)
        {
            if(feof(imu_file))
            {
                printf("have no imu_data that the timestamp is larger than the first frames!");
                exit(-1);
            }

            pre_imu_data = imu_data;
            
            if(EUROC)
            {
                double timestamp_euroc, ax_euroc, ay_euroc, az_euroc, wx_euroc, wy_euroc, wz_euroc;
                fscanf(imu_euroc_file, "%lf, %lf, %lf, %lf, %lf, %lf, %lf", &timestamp_euroc, &wx_euroc, &wy_euroc, &wz_euroc, &ax_euroc, &ay_euroc, &az_euroc);
                printf("%lf", timestamp_euroc);

                imu_data.timestamp = timestamp_euroc / time_stamp_to_sec;
                imu_data.acc << ax_euroc, ay_euroc, az_euroc;
                imu_data.gyr << wx_euroc, wx_euroc, wz_euroc;
            }
            else
            {
                double timestamp, ax, ay, az, wx, wy, wz;
                fscanf(imu_file, "%lf %lf %lf %lf %lf %lf %lf", &timestamp, &wx, &wy, &wz, &ax, &ay, &az);

                imu_data.timestamp = timestamp / time_stamp_to_sec;
                imu_data.acc << ax, ay, az;
                imu_data.gyr << wx, wy, wz;
            }

        }
        else
            break;
    }

    printf("start from the frame %d\n", frame_start);

    // cout << fixed << setprecision(30) << imu_data.timestamp << endl;
    // cout << fixed << setprecision(30) << frames[frame_start].timestamp << endl;

    bool init_flag = false;
    Initializer initializer;
    while(!init_flag)
    {   
        if(frame_start + num_for_init >= frames.size() - 1)
        {
            printf("frame_start + num_for_init = %d + %d\n", frame_start, num_for_init);
            printf("have no frames that meet the observibility\n");
            break;
            //return false;
        }
        IMUProcess imu_process;
        imu_process.ba = frames[frame_start + num_for_init].imu_process.ba;
        imu_process.bg = frames[frame_start + num_for_init].imu_process.bg;

        //compute the imu data at time frames[i].timestamp by interpolation
        IMUData interpolation = Interpolation(frames[frame_start + num_for_init].timestamp, pre_imu_data, imu_data);

        imu_process.imu_data.push_back(interpolation);

        while(imu_data.timestamp < frames[frame_start + num_for_init + 1].timestamp)
        {
            pre_imu_data = imu_data;
            
            if(feof(imu_file))
            {
                printf("have no enough imu data to initialize the system!");
                exit(-1);
            }

            if(EUROC)
            {
                imu_process.imu_data.push_back(pre_imu_data);
                
                double timestamp_euroc, ax_euroc, ay_euroc, az_euroc, wx_euroc, wy_euroc, wz_euroc;
                fscanf(imu_euroc_file, "%lf, %lf, %lf, %lf, %lf, %lf, %lf", &timestamp_euroc, &wx_euroc, &wy_euroc, &wz_euroc, &ax_euroc, &ay_euroc, &az_euroc);
                imu_data.timestamp = timestamp_euroc / time_stamp_to_sec;
                imu_data.acc << ax_euroc, ay_euroc, az_euroc;
                imu_data.gyr << wx_euroc, wx_euroc, wz_euroc;
            }
            else
            {
                //bug*****
                imu_process.imu_data.push_back(pre_imu_data);

                double timestamp, ax, ay, az, wx, wy, wz;
                fscanf(imu_file, "%lf %lf %lf %lf %lf %lf %lf", &timestamp, &wx, &wy, &wz, &ax, &ay, &az);
                imu_data.timestamp = timestamp / time_stamp_to_sec;
                imu_data.acc << ax, ay, az;
                imu_data.gyr << wx, wy, wz;
            }
            
        }

        interpolation = Interpolation(frames[frame_start + num_for_init + 1].timestamp, pre_imu_data, imu_data);
        imu_process.imu_data.push_back(interpolation);

        // for(int k = 0;k < imu_process.imu_data.size();k++)
        // {
        //     cout << fixed << setprecision(19) << imu_process.imu_data[k].timestamp << endl;
        // }
        // cout << endl;

        imu_process.PreIntegrate();

        frames[frame_start + num_for_init].imu_process = imu_process;

        // for(int k = 0;k < imu_process.imu_data.size();k++)
        // {
        //     cout << fixed << "imu timestamp: " << imu_process.imu_data[k].timestamp << " " << imu_process.imu_data[k].acc.transpose() << " " << imu_process.imu_data[k].gyr.transpose() << endl;
        // }
        
        initializer.addFrame(frames[frame_start + num_for_init]);

        init_flag = initializer.initialize();

        num_for_init++;
        cout << num_for_init << endl;
    }
    

    // check the data
    for(int i = 0;i < initializer.frames.size();i++)
    {
        //cout << "main() : img timestamp: " << initializer.frames[i].timestamp << endl;
        cout << "main() : " << i << "th frames" << endl;
        vector<IMUData> imudatas = initializer.frames[i].imu_process.imu_data;
        for(int j = 0;j < imudatas.size();j++)
        {
            cout << fixed << "main() : imu timestamp: " << imudatas[j].timestamp << " " << imudatas[j].acc.transpose() << " " << imudatas[j].gyr.transpose() << endl; 
        }
    }

    // for(int i = 0;i < initializer.frames.size();i++)
    // {
    //     cout << "main() : ba" << i << " = " << frames[frame_start + i].imu_process.ba.transpose() << endl;
    //     cout << "main() : bg" << i << " = " << frames[frame_start + i].velocity.transpose() << endl;
    // }

    for(int i = 0;i < initializer.frames.size();i++)
        frames[frame_start + i] = initializer.frames[i];

    Rbc = initializer.Rbc;
    Pbc = initializer.Pbc;
    g = initializer.g;
    s = initializer.s;

    cout << "Rbc = " << Rbc << endl;
    cout << "eulerAngles = "<<Rbc.eulerAngles(2, 1, 0);
    cout << "Pbc = " << Pbc.transpose() << endl;
    cout << "s = " << s << endl;

    // 1403636601763555584,0.0020943951023931952,0.018849555921538759,0.085172067497323284,9.3816951666666668,-0.5475379583333333,-2.6314510833333329
    // Vector3d acc;
    // acc << 9.3816951666666668,-0.5475379583333333,-2.6314510833333329;
    // int i = 0;
    // double the_timestamp = 1403636601763555584 / time_stamp_to_sec;
    // for(i = 0;i < frames.size();i++)
    //     if((frames[i].timestamp - the_timestamp) < 0.05 && (frames[i].timestamp - the_timestamp) > -0.05)
    //         break;

    //g = frames[i].R * acc;

    //for(int i = frame_start + num_for_init - WINDOW_SIZE;i < frame_start + num_for_init - 1;i++)
    for(int i = frame_start;i < frame_start + num_for_init - 1;i++)
    {
        Quaterniond q(frames[i].R);
        publishPath(frames[i].P.x(), frames[i].P.y(), frames[i].P.z(), q.w(), q.x(), q.y(), q.z());
        usleep(100000);
    }

    Vector3d P0, V0;
    Matrix3d R0; 
    if(initializer.frames.size() != 0)
    {
        P0 = frames[frame_start + num_for_init - WINDOW_SIZE].P - frames[frame_start + num_for_init - WINDOW_SIZE].R * Rbc.transpose() * Pbc;
        V0 = frames[frame_start + num_for_init - WINDOW_SIZE].velocity;
        R0 = frames[frame_start + num_for_init - WINDOW_SIZE].R * Rbc.transpose();
    }
    Quaterniond q(R0);
    publishPath(P0.x(), P0.y(), P0.z(), q.w(), q.x(), q.y(), q.z());
    
    for(int i = frame_start + num_for_init - WINDOW_SIZE;i < frame_start + num_for_init - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        P0 = P0 + frames[i].velocity * dt - 0.5 * g * dt * dt + R0 * frames[i].imu_process.pre_integration.delta_p;
        //R0 = R0 * frames[i].imu_process.pre_integration.delta_q;
        R0 = frames[i].R * Rbc.transpose();

        Vector3d P_camera = P0 + R0 * Pbc;

        Quaterniond q(R0);
        publishPath(P_camera.x(), P_camera.y(), P_camera.z(), q.w(), q.x(), q.y(), q.z());

        usleep(100000);
    }


    // Matrix3d R0 = initializer.frames[0].R * Rbc.transpose();
    // Vector3d P0 = initializer.frames[0].P - R0 * Pbc;
    // Quaterniond q0(R0);
    // publishPath(P0.x(), P0.y(), P0.z(), q0.w(), q0.x(), q0.y(), q0.z());
    // Matrix3d Rx1 = R0;
    // Vector3d Px1 = P0;
    // for(int i = 0;i < num_for_init - 1;i++)
    // {
    //     usleep(10000);
    //     double dt = initializer.frames[i + 1].timestamp - initializer.frames[i].timestamp;

    //     Px1 = Px1 + (frames[i].velocity * dt + Rx1 * initializer.frames[i].imu_process.pre_integration.delta_p - 0.5 * g * dt * dt) / s;
    //     Rx1 = Rx1 * initializer.frames[i].imu_process.pre_integration.delta_q.toRotationMatrix();
    //     Quaterniond q(Rx1);    
    //     publishPath(Px1.x(), Px1.y(), Px1.z(), q.w(), q.x(), q.y(), q.z());

    //     cout << "P_imu " << Px1.transpose() << endl;
    //     //cout << Rx * frames[i].velocity * dt + Rx * initializer.frames[i].imu_process.pre_integration.delta_p - 0.5 * g * dt * dt;

    // }

    ofstream bg_info;
    bg_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/without_optimize_pcb/bg.txt", ofstream::out);

    ofstream Rbc_info;
    Rbc_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/without_optimize_pcb/Rbc_angleAxis.txt", ofstream::out);

    ofstream Rbc_info_change;
    Rbc_info_change.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/without_optimize_pcb/Rbc_angleAxis_change.txt", ofstream::out);

    ofstream a_info;
    a_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/imu_integration/arr.txt", ofstream::out);

    ofstream v_info;
    v_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/imu_integration/velocity.txt", ofstream::out);

    ofstream ba_info;
    ba_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/imu_integration/ba.txt", ofstream::out);

    ofstream v_truth_info;
    v_truth_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/imu_integration/v_truth.txt", ofstream::out);

    ofstream v_residual_info;
    v_residual_info.open("/home/likaiyue/test/VI_Calibration_Result/test_data_for_matlab/imu_integration/v_residual_info.txt", ofstream::out);

    // Matrix3d R0 = frames[frame_start].R * Rbc.transpose();
    // Vector3d P0 = frames[frame_start].P - R0 * Pbc;
    // Vector3d V0 = frames[frame_start].velocity;

    // Quaterniond q0(R0);
    // publishPath(P0.x(), P0.y(), P0.z(), q0.w(), q0.x(), q0.y(), q0.z());
    
    // Matrix3d Rx(R0);
    // Vector3d Px(P0);
    // Vector3d Vx(V0);

    // cout << Vx << endl;
    //exit(-1);

    //g << -0.132303, -8.6587, -4.688;

    vector<Frame> optimized_frames;
    Vector3d Px, Vx;
    Matrix3d Rx;
    //sliding window
    int first_optimize = 1;
    //[frame_start, frame_start+1, frame_start+2, ... , frames_start+num_for_init-1]
    //*****
    Optimizer optimizer(frames, frame_start + num_for_init - WINDOW_SIZE, frame_start + num_for_init - 1, Rbc, Pbc, g);

    //check the data
    for(int i = 0;i < optimizer.frames.size();i++)
    {
        cout << "main() : velocity = " << optimizer.frames[i].velocity.transpose() << endl;
        cout << "main() : ba = " << optimizer.frames[i].imu_process.ba.transpose() << endl;
        cout << "main() : bg = " << optimizer.frames[i].imu_process.bg.transpose() << endl;
        cout << endl;
    }

    for(int i = frame_start + num_for_init;i < frames.size() - 1;i++)
    {   
        cout << i << "th frame are used to optimized. " << endl;

        IMUProcess imu_process(frames[i - 1].imu_process.ba, frames[i - 1].imu_process.bg);

        IMUData interpolation = Interpolation(frames[i].timestamp, pre_imu_data, imu_data);

        imu_process.imu_data.push_back(interpolation);

        while(imu_data.timestamp < frames[i + 1].timestamp)
        {
            pre_imu_data = imu_data;
            
            if(EUROC)
            {
                if(feof(imu_euroc_file))
                {
                    printf("have no imu data, the system exit!");
                    exit(-1);
                }
                
                imu_process.imu_data.push_back(pre_imu_data);
                
                double timestamp_euroc, ax_euroc, ay_euroc, az_euroc, wx_euroc, wy_euroc, wz_euroc;
                fscanf(imu_euroc_file, "%lf, %lf, %lf, %lf, %lf, %lf, %lf", &timestamp_euroc, &wx_euroc, &wy_euroc, &wz_euroc, &ax_euroc, &ay_euroc, &az_euroc);
                imu_data.timestamp = timestamp_euroc / time_stamp_to_sec;
                imu_data.acc << ax_euroc, ay_euroc, az_euroc;
                imu_data.gyr << wx_euroc, wx_euroc, wz_euroc;
            }
            else
            {
                if(feof(imu_file))
                {
                    printf("have no imu data, the system exit!");
                    exit(-1);
                }
                
                imu_process.imu_data.push_back(pre_imu_data);
                
                double timestamp, ax, ay, az, wx, wy, wz;
                fscanf(imu_file, "%lf %lf %lf %lf %lf %lf %lf", &timestamp, &wx, &wy, &wz, &ax, &ay, &az);
                imu_data.timestamp = timestamp / time_stamp_to_sec;
                imu_data.acc << ax, ay, az;
                imu_data.gyr << wx, wy, wz;
            }
        }

        interpolation = Interpolation(frames[i + 1].timestamp, pre_imu_data, imu_data);
        imu_process.imu_data.push_back(interpolation);

        imu_process.PreIntegrate();
        frames[i].imu_process = imu_process;
        frames[i].velocity = frames[i - 1].velocity;

        // cout << "P[" << i << "] = " << (frames[i].P - frames[i - 1].P).transpose() << endl;
        // cout << "distant[" << i << "] = " << (frames[i].P - frames[i - 1].P).transpose() * (frames[i].P - frames[i - 1].P) << endl;
        // continue;

        //marginalize the last second frame
        //if((frames[i].P - frames[i - 1].P).transpose() * (frames[i].P - frames[i - 1].P) < 0.0001)
        if(false)
        {   
            cout << "marginalize the last second frame." << endl;

            optimizer.UpdateWindow(frames[i], 1);  
            optimizer.Optimize_Ceres(first_optimize);
        }
        else //marginalize the first frame
        {
            //cout << "Jacobian = " << frames[i].imu_process.pre_integration.jacobian << endl;
            //cout << "covariance = " << frames[i].imu_process.pre_integration.covariance << endl;

            // for(int j = i - WINDOW_SIZE;j <= i;j++)
            // {
            //     cout << frames[i].imu_process.ba << " " << frames[i].imu_process.bg << endl;
            // }

            //fixed the i - WINDOW_SIZE frames(ba bg )
            optimizer.UpdateWindow(frames[i], 2);
            optimizer.Optimize_Ceres(first_optimize);
            //optimizer.Optimize_Ceres_before();
            //optimizer.OptimizeRbc_Ceres();

            //the first frame in sliding window is fixed as the prior information, delete it from sliding window after optimization
            optimized_frames.push_back(optimizer.frames[0]);
            //cout << "=====================" << optimizer.frames[0].velocity.transpose() << endl;
            optimizer.frames.erase(optimizer.frames.begin());
            //cout << optimized_frames.back().velocity.transpose() << endl;


            //double dt= frames[i - WINDOW_SIZE + 1].timestamp - frames[i - WINDOW_SIZE].timestamp;
            // Px = Px + Vx * dt + Rx * frames[i - WINDOW_SIZE].imu_process.pre_integration.delta_p - 0.5 * g * dt * dt;
            // Vx = Vx + Rx * frames[i - WINDOW_SIZE].imu_process.pre_integration.delta_v - g * dt;
            // cout << "Delta_V" << i - WINDOW_SIZE << " " << (frames[i - WINDOW_SIZE].imu_process.pre_integration.delta_v - g * dt).transpose() << endl;
            //Vx = frames[i - WINDOW_SIZE + 1].velocity;
            //Rx = Rx * frames[i - WINDOW_SIZE].imu_process.pre_integration.delta_q; 
            //Rx = frames[i - WINDOW_SIZE + 1].R * Rbc.transpose();
            
            if(first_optimize == 1)
            {
                Px = optimized_frames.back().P - optimized_frames.back().R * Rbc.transpose() * Pbc;
                Rx = optimized_frames.back().R * Rbc.transpose();
                Vx = optimized_frames.back().velocity;
                
                Quaterniond q0(Rx);
                publishPath(Px.x(), Px.y(), Px.z(), q0.w(), q0.x(), q0.y(), q0.z());
            }
            
            // cout << dt << endl;
            // cout << "Px " << Px.transpose();
            // cout << "Vx " << Vx.transpose();
            // cout << "Rx " << Rx.transpose();

            a_info << i - WINDOW_SIZE << " " << optimized_frames.back().timestamp << " " << (optimized_frames.back().R * Rbc.transpose() * (optimized_frames.back().imu_process.imu_data[0].acc - optimized_frames.back().imu_process.ba) - g).transpose() << endl;
            v_info << i - WINDOW_SIZE << " " << optimized_frames.back().timestamp << " " << Vx.transpose() << endl;
            ba_info << i - WINDOW_SIZE << " " << optimized_frames.back().timestamp << " " << optimized_frames.back().imu_process.ba.transpose() << endl;
            v_truth_info << i - WINDOW_SIZE << " " << optimized_frames.back().timestamp << " " << optimized_frames.back().velocity.transpose() << endl; 
            
            double dt = optimizer.frames[0].timestamp - optimized_frames.back().timestamp;
            Vector3d v_residual = Rbc * optimized_frames.back().R.transpose() * (optimizer.frames[0].velocity - optimized_frames.back().velocity + g * dt) - optimized_frames.back().imu_process.pre_integration.delta_v; 
            v_residual_info << i - WINDOW_SIZE << " " << optimized_frames.back().timestamp << " "  << v_residual.transpose() << endl;
            // exit(-1);
            
            Px = Px + optimized_frames.back().velocity * dt + Rx * optimized_frames.back().imu_process.pre_integration.delta_p - 0.5 * g * dt * dt;
            Vx = Vx + Rx * optimized_frames.back().imu_process.pre_integration.delta_v - g * dt;
            Rx = Rx * optimized_frames.back().imu_process.pre_integration.delta_q;
            Quaterniond q(Rx);
            publishPath(Px.x(), Px.y(), Px.z(), q.w(), q.x(), q.y(), q.z());

            cout << "frame " << i << " has been optimized!" << endl;

            cout << "optimized_velocity = " << optimized_frames.back().velocity << "------------------------" << endl;

            first_optimize = 0;
        }

        Matrix3d pre_Rbc = Rbc;
        Rbc = optimizer.Rbc;
        Pbc = - Rbc * optimizer.Pcb;

        cout << "main() : optimiztion Pbc = " << Pbc.transpose() << endl;
        cout << "main() : optimiztion Rbc = " << Rbc << endl;

        bg_info << i - WINDOW_SIZE<< " ";
        bg_info << fixed << setprecision(16) << frames[i - WINDOW_SIZE].timestamp;
        bg_info << " " << frames[i - WINDOW_SIZE].imu_process.bg.transpose() << endl;


        Rbc_info << i - WINDOW_SIZE << " ";
        Rbc_info << fixed << setprecision(16) << frames[i - WINDOW_SIZE].timestamp;
        Rbc_info << " " << AngleAxisd(Rbc).angle() << endl;

        Rbc_info_change << i - WINDOW_SIZE<< " ";
        Rbc_info_change << fixed << setprecision(16) << frames[i - WINDOW_SIZE].timestamp;
        Rbc_info_change << " " << AngleAxisd(Rbc * pre_Rbc.transpose()).angle() << endl;
        
    }


    for(int i = frame_start;i < frame_start + 42;i++)
    {
        cout << "frames" << i << " " << (Rbc * frames[i].R.transpose() * g - frames[i].imu_process.imu_data[0].acc).transpose() << endl;
    }

    for(int i = 30;i < 40;i++)
    {
        for(int j = 0;j < 5;j++)
            cout << frames[i].imu_process.imu_data[j].acc.transpose() << endl;
        cout << endl;
    }

    Vector3d g_tmp = Vector3d::Zero();
    int count = 0;
    for(int i = 30;i < 36;i++)
    {
        for(int j = 0;j < frames[i].imu_process.imu_data.size();j++)
        {
            g_tmp += frames[i].R * frames[i].imu_process.imu_data[j].acc;
            count++;
        }
    }
    g = g_tmp / count;
    cout << g << endl;

    v_info.close();
    v_truth_info.close();
    ba_info.close();
    bg_info.close();
    fclose(imu_file);

    return 0;
}
