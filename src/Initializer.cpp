#include"Initializer.h"
#include"parameter.h"
#include"Utility.h"
#include<iostream>
#include<stdio.h>

Initializer::Initializer()
{
    ba << 0.0, 0.0, 0.0;
    bg << 0.0, 0.0, 0.0;

    Rbc = Matrix3d::Identity();
    Pbc = Vector3d::Zero();

}

void Initializer::addFrame(Frame frame)
{
    frames.push_back(frame);
}

bool Initializer::computeRbc()
{
    if(frames.size() == 1)
        return false;

    MatrixXd A((frames.size() - 1) * 4, 4);
    A.setZero();

    Rc_g.push_back(Rbc.transpose() * frames[frames.size() - 2].imu_process.pre_integration.delta_q.toRotationMatrix() * Rbc);

    for(int i = 0;i < frames.size() - 1;i++)
    {   
        // cout << "Rbi_i+1 " << i << " = " << frames[i].imu_process.pre_integration.delta_q.coeffs().transpose() << endl;
        // cout <<  "Rci_i+1 " << i << " = " << Quaterniond(frames[i].R.transpose() * frames[i + 1].R).coeffs().transpose() << endl;
        
        auto matri = frames[i].imu_process.pre_integration.delta_q.toRotationMatrix();
        //cout << i <<" imu: " << matri.eulerAngles(2, 1, 0) << endl;
        //cout << i << " camera: " << (frames[i].R.transpose() * frames[i + 1].R).eulerAngles(2, 1, 0) << endl;

        Matrix3d dRc_g = Rc_g[i];
    
        //compute huber
        Quaterniond r1(frames[i].R.transpose() * frames[i + 1].R);
        Quaterniond r2(dRc_g);
        //Quaterniond r2(Rbc.transpose() * frames[i].imu_process.pre_integration.delta_q * Rbc);
        double angular_distance = 180 / M_PI * r1.angularDistance(r2);
        //cout << "Quaterniond_distance = " << r1.angularDistance(r2) << endl;
        //cout << "angular_distance = " << angular_distance << endl;
        double huber = angular_distance > 5 ? 5 / angular_distance : 1.0;
        huber = 1.0;
        Matrix4d L, R;

        // double w = Quaterniond(frames[i].R.transpose() * frames[i + 1].R).w();
        // Vector3d q = Quaterniond(frames[i].R.transpose() * frames[i + 1 ].R).vec();

        // R.block<3, 3>(0, 0) = w * Matrix3d::Identity() - skewSymmetric(q);
        // R.block<3, 1>(0, 3) = q;
        // R.block<1, 3>(3, 0) = -q.transpose();
        // R(3, 3) = w;

        // w = frames[i].imu_process.pre_integration.delta_q.w();
        // q = frames[i].imu_process.pre_integration.delta_q.vec();
        // L.block<3, 3>(0, 0) = w * Matrix3d::Identity() + skewSymmetric(q);
        // L.block<3, 1>(0, 3) = q;
        // L.block<1, 3>(3, 0) = -q.transpose();
        // L(3, 3) = w;


        double w = Quaterniond(frames[i].R.transpose() * frames[i + 1].R).w();
        Vector3d q = Quaterniond(frames[i].R.transpose() * frames[i + 1].R).vec();
        L.block<3, 3>(0, 0) = w * Matrix3d::Identity() + skewSymmetric(q);
        L.block<3, 1>(0, 3) = q;
        L.block<1, 3>(3, 0) = -q.transpose();
        L(3, 3) = w;

        Quaterniond R_ij = frames[i].imu_process.pre_integration.delta_q;
        w = R_ij.w();
        q = R_ij.vec();
        R.block<3, 3>(0, 0) = w * Matrix3d::Identity() - skewSymmetric(q);
        R.block<3, 1>(0, 3) = q;
        R.block<1, 3>(3, 0) = -q.transpose();
        R(3, 3) = w;

        A.block<4, 4>(i * 4, 0) = huber * (L -R);
    }
    //cout << A << endl;

    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV);
    Matrix<double, 4, 1> x = svd.matrixV().col(3);
    cout << x << endl;
    Quaterniond estimated_R(x);
    cout << "estimated_qbc: "<< estimated_R.coeffs() << endl;
    cout << "estimated_qbc_normalized() = " << estimated_R.normalized().coeffs().transpose();
    Rbc = estimated_R.toRotationMatrix().inverse();
    
    AngleAxisd aa1(Rbc);
    cout << "AngleAxisd = " << aa1.axis().transpose() << endl;
    cout << "angle: "<< aa1.angle() << endl;

    //cout << svd.singularValues().transpose() << endl;
    //cout << ric << endl;

    VectorXd singular = svd.singularValues().transpose();
    std::cout << "A_singular = " << singular << endl;
    std::cout << "Rbc = " << Rbc << endl;

    if (singular(2) > 0.10 && frames.size() >= MIN_FRAME_NUM)
    {
        return true;
    }
    else
        return false;
}

void Initializer::computeBg()
{
    Matrix3d A;
    A.setZero();
    Vector3d b;
    b.setZero();

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Matrix3d R_i = frames[i].R * Rbc.transpose();
        Matrix3d R_i_next = frames[i + 1].R * Rbc.transpose();
        Quaterniond qij(R_i.transpose() * R_i_next);
        Vector3d temp_b = 2 * (frames[i].imu_process.pre_integration.delta_q.inverse() * qij).vec();
        Matrix3d J = frames[i].imu_process.pre_integration.jacobian.block<3, 3>(3, 12);

        A += J.transpose() * J;     
        b += J.transpose() * temp_b;
    }

    bg = A.ldlt().solve(b);

    for(int i = 0;i < frames.size();i++)
    {
        frames[i].imu_process.UpdatePreIntegration(ba, bg);
    }

    cout << "bg = " << bg << endl;
}


bool Initializer::computeGravityVelocity()
{
    int all_frame_count = frames.size();
    int n_state = 3 * all_frame_count + 3;

    MatrixXd A(n_state, n_state);
    A.setZero(); 
    VectorXd b(n_state);
    b.setZero();

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Matrix<double, 6, 9> temp_A;
        temp_A.setZero();
        Matrix<double, 6, 1> temp_b;
        temp_b.setZero();

        double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;

        temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
        temp_A.block<3, 3>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * sum_dt * sum_dt;
        temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p - Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P)
                                    + Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose() * Pbc - Pbc;        
        
        temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
        temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_A.block<3, 3>(3, 6) = Rbc * frames[i].R.transpose() * sum_dt;
        temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v;

        Matrix<double, 6, 6> cov_inv;
        cov_inv.setIdentity();

        MatrixXd r_A = temp_A.transpose() * cov_inv * temp_A;
        VectorXd r_b = temp_A.transpose() * cov_inv * temp_b;

        A.block<6, 6>(i * 3, i * 3) += r_A.block<6, 6>(0, 0);
        A.bottomRightCorner<3, 3>() += r_A.bottomRightCorner<3, 3>();

        A.block<6, 3>(i * 3, n_state - 3) += r_A.block<6, 3>(0, 6);
        A.block<3, 6>(n_state - 3, i * 3) += r_A.block<3, 6>(6, 0);

        b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);
        b.tail<3>() += r_b.tail<3>(); 
    }

    A = A * 1000.0;
    b = b * 1000.0;

    VectorXd x = A.ldlt().solve(b);
    g = x.tail<3>();
    s = 1;

    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d velocity_i = x.block<3, 1>(i * 3, 0);
        Vector3d velocity_j = x.block<3, 1>((i + 1)* 3, 0);
        
        Vector3d imu_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p + frames[i].R * Rbc.transpose() * velocity_i * dt - 0.5 * g * dt * dt;
        Vector3d dist_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p - 
                            (frames[i + 1].P - frames[i].P + 0.5 * g * dt * dt - frames[i].R * Rbc.transpose() * velocity_i * dt);
        Vector3d dist_v = frames[i].R * Rbc.transpose() *frames[i].imu_process.pre_integration.delta_v - 
                            (frames[i + 1].R * Rbc.transpose() * velocity_j - frames[i].R * Rbc.transpose() * velocity_i + g * dt);

        // cout << "p: " << (frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - frames[i].P + frames[i].R * Rbc.transpose() * Pbc).transpose() << endl;
        // cout << "imu_p: " << imu_p.transpose() << endl;
        // cout << "preintegration_p: " << frames[i].imu_process.pre_integration.delta_p.transpose() << endl;
        // cout << "dist_p: " << dist_p.transpose() << endl;
        // cout << "dist_v: " << dist_v.transpose() << endl;
        // cout << endl;
    }

    cout << "g = " << g << endl;
    cout << "g.norm() = " << g.norm() << endl;
    // cout << "b = " << b << endl;

    for(int i = 0;i < frames.size();i++)
    {
        frames[i].velocity = frames[i].R * Rbc.transpose() * x.segment<3>(i * 3);
        //cout << "estimation velocity: " << frames[i].velocity.transpose() << endl;
    }

    if(fabs(g.norm() - G.norm()) > 1.0)
        return false;
    else
    {         
        RefineGravityVelocity();
        return true;
    }
}

bool Initializer::computeGravityVelocityPbc()
{
    int all_frame_count = frames.size();
    int n_state = 3 * all_frame_count + 3 + 3;

    MatrixXd A(n_state, n_state);
    A.setZero(); 
    VectorXd b(n_state);
    b.setZero();

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Matrix<double, 6, 12> temp_A;
        temp_A.setZero();
        Matrix<double, 6, 1> temp_b;
        temp_b.setZero();

        double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;

        temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
        temp_A.block<3, 3>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * sum_dt * sum_dt;
        temp_A.block<3, 3>(0, 9) = Matrix3d::Identity() - Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p - Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P);        
        
        temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
        temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_A.block<3, 3>(3, 6) = Rbc * frames[i].R.transpose() * sum_dt;
        temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v;

        Matrix<double, 6, 6> cov_inv;
        cov_inv.setIdentity();

        MatrixXd r_A = temp_A.transpose() * cov_inv * temp_A;
        VectorXd r_b = temp_A.transpose() * cov_inv * temp_b;

        A.block<6, 6>(i * 3, i * 3) += r_A.block<6, 6>(0, 0);
        A.bottomRightCorner<6, 6>() += r_A.bottomRightCorner<6, 6>();

        A.block<6, 6>(i * 3, n_state - 6) += r_A.block<6, 6>(0, 6);
        A.block<6, 6>(n_state - 6, i * 3) += r_A.block<6, 6>(6, 0);

        b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);
        b.tail<6>() += r_b.tail<6>(); 
    }

    A = A * 1000.0;
    b = b * 1000.0;

    VectorXd x = A.ldlt().solve(b);
    g = x.segment<3>(n_state - 6);
    Pbc = x.tail<3>();
    s = 1;

    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d velocity_i = x.block<3, 1>(i * 3, 0);
        Vector3d velocity_j = x.block<3, 1>((i + 1)* 3, 0);
        
        Vector3d imu_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p + frames[i].R * Rbc.transpose() * velocity_i * dt - 0.5 * g * dt * dt;
        Vector3d dist_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p - 
                            (frames[i + 1].P - frames[i].P + 0.5 * g * dt * dt - frames[i].R * Rbc.transpose() * velocity_i * dt);
        Vector3d dist_v = frames[i].R * Rbc.transpose() *frames[i].imu_process.pre_integration.delta_v - 
                            (frames[i + 1].R * Rbc.transpose() * velocity_j - frames[i].R * Rbc.transpose() * velocity_i + g * dt);

        cout << "p: " << (frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - frames[i].P + frames[i].R * Rbc.transpose() * Pbc).transpose() << endl;
        cout << "imu_p: " << imu_p.transpose() << endl;
        cout << "preintegration_p: " << frames[i].imu_process.pre_integration.delta_p.transpose() << endl;
        cout << "dist_p: " << dist_p.transpose() << endl;
        cout << "dist_v: " << dist_v.transpose() << endl;
        cout << endl;
    }

    cout << "g = " << g << endl;
    cout << "g.norm() = " << g.norm() << endl;
    cout << "b = " << b << endl;

    if(fabs(g.norm() - G.norm()) > 1.0)
        return false;
    else
    {         
        RefineGravityVelocity();
        return true;
    }
}


bool Initializer::computeGravityVelocityScale()
{
    int all_frame_count = frames.size();
    int n_state = 3 * all_frame_count + 4;

    MatrixXd A(n_state, n_state);
    A.setZero(); 
    VectorXd b(n_state);
    b.setZero();

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Matrix<double, 6, 10> temp_A;
        temp_A.setZero();
        Matrix<double, 6, 1> temp_b;
        temp_b.setZero();

        double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;

        temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
        temp_A.block<3, 3>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * sum_dt * sum_dt;
        temp_A.block<3, 1>(0, 9) = Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P);
        temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p + (Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose()) * Pbc - Pbc;        
        
        temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
        temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_A.block<3, 3>(3, 6) = Rbc * frames[i].R.transpose() * sum_dt;
        temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v;

        Matrix<double, 6, 6> cov_inv;
        cov_inv.setZero();
        cov_inv(0, 0) = 1;
        cov_inv(1, 1) = 1;
        cov_inv(2, 2) = 1;
        cov_inv(3, 3) = 1;
        cov_inv(4, 4) = 1;
        cov_inv(5, 5) = 1;
        //cov_inv.setIdentity();

        MatrixXd r_A = temp_A.transpose() * cov_inv * temp_A;
        VectorXd r_b = temp_A.transpose() * cov_inv * temp_b;

        A.block<6, 6>(i * 3, i * 3) += r_A.topLeftCorner<6, 6>();
        b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);

        A.bottomRightCorner<4, 4>() += r_A.bottomRightCorner<4 ,4>();
        b.tail<4>() += r_b.tail<4>(); 

        A.block<6, 4>(i * 3, n_state - 4) += r_A.block<6, 4>(0, 6);
        A.block<4, 6>(n_state - 4, i * 3) += r_A.block<4, 6>(6, 0);
    }

    // A = A * 1000.0;
    // b = b * 1000.0;

    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV);
    VectorXd sv = svd.singularValues();
    cout << "sv = " << sv << "---------------------------" << endl;

    VectorXd x = A.ldlt().solve(b);
    first_g = x.segment<3>(n_state - 4);
    g = first_g;
    s = x(n_state - 1);

    cout << "g = " << g << endl;
    cout << "g.norm() = " << g.norm() << endl;

    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d velocity_i = x.block<3, 1>(i * 3, 0);
        Vector3d velocity_j = x.block<3, 1>((i + 1)* 3, 0);
        
        Vector3d imu_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p + frames[i].R * Rbc.transpose() * velocity_i * dt - 0.5 * g * dt * dt;
        Vector3d residual_p = imu_p - (s * frames[i + 1].P - s * frames[i].P);
        Vector3d residual_v = frames[i].imu_process.pre_integration.delta_v - 
                            Rbc * frames[i].R.transpose() * (frames[i + 1].R * Rbc.transpose() * velocity_j - frames[i].R * Rbc.transpose() * velocity_i + g * dt);

        cout << "p: " << (s * frames[i + 1].P - s * frames[i].P - frames[i + 1].R * Rbc.transpose() * Pbc + frames[i].R * Rbc.transpose() * Pbc).transpose() << endl;
        cout << "imu_p: " << imu_p.transpose() << endl;
        cout << "residual_p: " << residual_p.transpose() << endl;
        cout << "preintegration_p: " << frames[i].imu_process.pre_integration.delta_p.transpose() << endl;
        cout << "v: " << frames[i].R * Rbc.transpose() * velocity_i << endl;
        cout << "preintegration_v: " << frames[i].imu_process.pre_integration.delta_v.transpose() << endl;
        cout << "residual_v: " << residual_v.transpose() << endl;
        
        cout << endl;
    }

    cout << "g = " << g << endl;
    cout << "g.norm() = " << g.norm() << endl;
    cout << "b = " << b << endl;
    
    first_g = g;

    for(int i = 0;i < frames.size();i++)
    {
        frames[i].velocity = frames[i].R * Rbc.transpose() * x.segment<3>(i * 3);
        cout << "velocity:" << frames[i].velocity << endl;
    }

    if(fabs(g.norm() - G.norm()) > 1.0)
        return false;
    else
    {         
        RefineGravityVelocity();
        return true;
    }
}

bool Initializer::computeGravityVelocityPbcScale()
{
    int all_frame_count = frames.size();
    //vi + g + pbc + s
    int n_state = 3 * all_frame_count + 3 + 3 + 1;

    MatrixXd A(n_state, n_state);
    A.setZero(); 
    VectorXd b(n_state);
    b.setZero();

    for(int i = 0;i < frames.size() - 1;i++)
    {
        Matrix<double, 6, 13> temp_A;
        temp_A.setZero();
        Matrix<double, 6, 1> temp_b;
        temp_b.setZero();

        double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;

        temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
        temp_A.block<3, 3>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * sum_dt * sum_dt;
        temp_A.block<3, 3>(0, 9) = Matrix3d::Identity() - Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_A.block<3, 1>(0, 12) = Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P);
        temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p;        
        
        temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
        temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
        temp_A.block<3, 3>(3, 6) = Rbc * frames[i].R.transpose() * sum_dt;
        temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v;

        Matrix<double, 6, 6> cov_inv;
        cov_inv.setIdentity();

        MatrixXd r_A = temp_A.transpose() * cov_inv * temp_A;
        VectorXd r_b = temp_A.transpose() * cov_inv * temp_b;

        A.block<6, 6>(i * 3, i * 3) += r_A.block<6, 6>(0, 0);
        b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);

        A.bottomRightCorner<7, 7>() += r_A.bottomRightCorner<7, 7>();
        b.tail<7>() += r_b.tail<7>(); 

        A.block<6, 7>(i * 3, n_state - 7) += r_A.block<6, 7>(0, 6);
        A.block<7, 6>(n_state - 7, i * 3) += r_A.block<7, 6>(6, 0);

        //cout << "temp_A = " << temp_A.block<3, 3>(0, 9) << endl;
    }

    A = A * 1000.0;
    b = b * 1000.0;

    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV);
    VectorXd sv = svd.singularValues();
    cout << "sv = " << sv << "---------------------------" << endl;

    VectorXd x = A.ldlt().solve(b);
    first_g = x.segment<3>(n_state - 7);
    g = first_g;
    first_Pbc = x.segment<3>(n_state - 4);
    Pbc = first_Pbc;

    s = x(n_state - 1);

    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d velocity_i = x.block<3, 1>(i * 3, 0);
        Vector3d velocity_j = x.block<3, 1>((i + 1)* 3, 0);
        
        Vector3d imu_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p + frames[i].R * Rbc.transpose() * velocity_i * dt - 0.5 * g * dt * dt;
        Vector3d dist_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p - 
                            (frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - (frames[i].P - frames[i].R * Rbc.transpose() * Pbc) + 
                                0.5 * g * dt * dt - frames[i].R * Rbc.transpose() * velocity_i * dt);
        Vector3d dist_v = frames[i].R * Rbc.transpose() *frames[i].imu_process.pre_integration.delta_v - 
                            (frames[i + 1].R * Rbc.transpose() * velocity_j - frames[i].R * Rbc.transpose() * velocity_i + g * dt);

        cout << "p: " << (s * frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - s * frames[i].P + frames[i].R * Rbc.transpose() * Pbc).transpose() << endl;
        cout << "imu_p: " << imu_p.transpose() << endl;
        cout << "preintegration_p: " << frames[i].imu_process.pre_integration.delta_p.transpose() << endl;
        cout << "dist_p: " << dist_p.transpose() << endl;
        cout << "dist_v: " << dist_v.transpose() << endl;
        cout << endl;
    }

    cout << "g = " << g << endl;
    cout << "g.norm() = " << g.norm() << endl;
    cout << "Pbc = " << Pbc << endl;
    //cout << "b = " << b << endl;

    if(fabs(g.norm() - G.norm()) > 1.0)
        return false;
    else
    {         
        RefineGravityVelocityPbc();
        return true;
    }
}

bool Initializer::ComputeGravityPcbScale_VIORB()
{   
    int n_state = 3 * (frames.size() - 2);
    MatrixXd A(n_state, 7);
    VectorXd b(n_state);

    for(int i = 0;i < frames.size() - 2;i++)
    {
        double t12 = frames[i + 1].timestamp - frames[i].timestamp;
        double t23 = frames[i + 2].timestamp - frames[i + 1].timestamp;

        Vector3d p12 = frames[i].imu_process.pre_integration.delta_p;
        Vector3d p23 = frames[i + 1].imu_process.pre_integration.delta_p;
        Vector3d v12 = frames[i + 1].imu_process.pre_integration.delta_v;

        Matrix3d Rc1 = frames[i].R;
        Matrix3d Rc2 = frames[i + 1].R;
        Matrix3d Rc3 = frames[i + 2].R;

        Vector3d Pc1 = frames[i].P;
        Vector3d Pc2 = frames[i + 1].P;
        Vector3d Pc3 = frames[i + 2].P;

        //lambda * g + beta * Pcb + erfa * s = gama
        Matrix3d lambda = 0.5 * Matrix3d::Identity() * (t12 * t12 * t23 + t23 * t23 * t12);
        Matrix3d beta = (Rc2 - Rc1) * t23 - (Rc3 - Rc2) * t12;
        Vector3d erfa = (Pc2 - Pc1) * t23 - (Pc3 - Pc2) * t12;

        Vector3d gamma = Rc1 * Rbc.transpose() * p12 * t23 - Rc2 * Rbc.transpose() * p23 * t12 - Rc1 * Rbc.transpose() * v12 * t12 * t23;

        A.block<3, 3>(3 * i, 0) = lambda;
        A.block<3, 3>(3 * i, 3) = beta;
        A.block<3, 1>(3 * i, 6) = erfa;
        b.segment<3>(3 * i) = gamma;
    }

    VectorXd x = A.jacobiSvd(ComputeFullU | ComputeFullV).solve(b);

    cout << "SVD_x = " << x << endl; 

    g = x.segment<3>(0);
    Pbc = x.segment<3>(3);
    s = x(6);

    return true;
}

bool Initializer::ComputeGravityScale_VIORB()
{   
    int n_state = 3 * (frames.size() - 2);
    MatrixXd A(n_state, 4);
    VectorXd b(n_state);

    Vector3d Pcb = -Rbc.transpose() * Pbc;

    for(int i = 0;i < frames.size() - 2;i++)
    {
        double t12 = frames[i + 1].timestamp - frames[i].timestamp;
        double t23 = frames[i + 2].timestamp - frames[i + 1].timestamp;

        Vector3d p12 = frames[i].imu_process.pre_integration.delta_p;
        Vector3d p23 = frames[i + 1].imu_process.pre_integration.delta_p;
        Vector3d v12 = frames[i + 1].imu_process.pre_integration.delta_v;

        Matrix3d Rc1 = frames[i].R;
        Matrix3d Rc2 = frames[i + 1].R;
        Matrix3d Rc3 = frames[i + 2].R;

        Vector3d Pc1 = frames[i].P;
        Vector3d Pc2 = frames[i + 1].P;
        Vector3d Pc3 = frames[i + 2].P;

        //lambda * g + erfa * s = gama
        Matrix3d lambda = 0.5 * Matrix3d::Identity() * (t12 * t12 * t23 + t23 * t23 * t12);
        Vector3d erfa = (Pc2 - Pc1) * t23 - (Pc3 - Pc2) * t12;

        Vector3d gamma = Rc1 * Rbc.transpose() * p12 * t23 - Rc2 * Rbc.transpose() * p23 * t12 - Rc1 * Rbc.transpose() * v12 * t12 * t23
                            - (Rc2 - Rc1) * Pcb * t23 + (Rc3 - Rc2) * Pcb * t12;

        A.block<3, 3>(3 * i, 0) = lambda;
        A.block<3, 1>(3 * i, 3) = erfa;
        b.segment<3>(3 * i) = gamma;
    }

    VectorXd x = A.jacobiSvd(ComputeFullU | ComputeFullV).solve(b);

    cout << "SVD_x = " << x << endl; 

    g = x.segment<3>(0);
    s = x(3);

    return true;
}


void Initializer::RefineGravityVelocity()
{
    cout << g.norm() << "\\\\\\\\\\\\\\\\\\\\\\\\\\\\" << endl;
    Vector3d g0 = g.normalized() * G.norm();

    // b1 << g0(1) * temp(2) - g0(2) * temp(1), g0(0) * temp(2) - g0(2) * temp(0), g0(0) - temp(1) - g0(1) * temp(0);
    // b1 = b1.normalized();
    // b2 = b1.cross(g.normalized());
    // cout << "b1 = " << b1 << endl;

    int n_state = frames.size() * 3 + 2;

    VectorXd x;
    
    int refine_times = 5;
    for(int k = 0;k < refine_times;k++)
    {        
        Vector3d b1, b2;
        Vector3d temp(1.0, 0.0, 0.0);
        if(temp == g.normalized())
            temp << 0.0, 0.0, 1.0;
        Vector3d a = g0.normalized();
        b1 = (temp - a * (a.transpose() * temp)).normalized();
        b2 = a.cross(b1);

        MatrixXd A(n_state, n_state);
        A.setZero();
        VectorXd b(n_state);
        b.setZero();

        for(int i = 0;i < frames.size() - 1;i++)
        {
            Matrix<double, 6, 8> temp_A;
            temp_A.setZero();
            Matrix<double, 6, 1> temp_b;
            temp_b.setZero();

            double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;
            temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
            temp_A.block<3, 1>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * b1 * sum_dt * sum_dt;
            temp_A.block<3, 1>(0, 7) = 0.5 * Rbc * frames[i].R.transpose() * b2 * sum_dt * sum_dt;
            temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p - Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P)
                                        + (Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose()) * Pbc - Pbc - 0.5 * Rbc * frames[i].R.transpose() * g0 * sum_dt * sum_dt;

            temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
            temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
            temp_A.block<3, 1>(3, 6) = Rbc * frames[i].R.transpose() * b1 * sum_dt;
            temp_A.block<3, 1>(3, 7) = Rbc * frames[i].R.transpose() * b2 * sum_dt;
            temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v - Rbc * frames[i].R.transpose() * g0 * sum_dt;
            
            Matrix<double, 6, 6> con_inv;
            con_inv.setIdentity();

            MatrixXd r_A = temp_A.transpose() * con_inv * temp_A;
            VectorXd r_b = temp_A.transpose() * con_inv * temp_b;

            A.block<6, 6>(i * 3, i * 3) += r_A.block<6, 6>(0, 0);
            A.bottomRightCorner<2, 2>() += r_A.block<2, 2>(6 ,6);
            A.block<6, 2>(i * 3, n_state - 2) += r_A.block<6, 2>(0, 6);
            A.block<2, 6>(n_state - 2, i * 3) += r_A.block<2, 6>(6, 0);
            
            b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);
            b.block<2 ,1>(n_state - 2, 0) += r_b.block<2, 1>(6 ,0);

            // cout << "temp_b = " << temp_b << endl;
            // cout << "r_b = " << r_b << endl;
            // cout << "b2 = " << b2 << endl;
            // cout << "b1 = " << b1 << endl;
        }

        A = A * 1000.0;
        b = b * 1000.0;
        x = A.ldlt().solve(b);
        VectorXd dg = x.tail<2>();
        g0 = (g0 + b1 * dg(0) + b2 * dg(1)).normalized() * G.norm();

        cout << "dg(0) = " << dg(0) << " " << "dg(1) = " << dg(1) << endl;
        //cout << "b = " << b << endl;
    }

    g = g0;
    cout << "g_refine = " << g.transpose() << endl;

    for(int i = 0;i < frames.size();i++)
    {
        cout << "RefineGravityVelocity() : estimation velocity: " << i << frames[i].velocity.transpose() << endl; 
        frames[i].velocity = frames[i].R * Rbc.transpose() * x.segment<3>(i * 3);   
        cout << "RefineGravityVelocity() : refinement_velocity: " << i << frames[i].velocity.transpose() << endl;
        cout << endl;
    }

    // for(int i = 0;i < frames.size() - 1;i++)
    // {
    //     double dt = frames[i + 1].timestamp - frames[i].timestamp;
    //     Vector3d velocity_i = x.block<3, 1>(i * 3, 0);
    //     Vector3d velocity_j = x.block<3, 1>((i + 1)* 3, 0);
        
    //     Vector3d imu_p = frames[i].R * Rbc.transpose() * frames[i].imu_process.pre_integration.delta_p + frames[i].R * Rbc.transpose() * velocity_i * dt - 0.5 * g * dt * dt;
    //     Vector3d dist_p = imu_p - (frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - frames[i].P + frames[i].R * Rbc.transpose() * Pbc);
    //     Vector3d dist_v = frames[i].R * Rbc.transpose() *frames[i].imu_process.pre_integration.delta_v - 
    //                         (frames[i + 1].R * Rbc.transpose() * velocity_j - frames[i].R * Rbc.transpose() * velocity_i + g * dt);

    //     cout << "p: " << (frames[i + 1].P - frames[i + 1].R * Rbc.transpose() * Pbc - frames[i].P + frames[i].R * Rbc.transpose() * Pbc).transpose() << endl;
    //     cout << "imu_p: " << imu_p.transpose() << endl;
    //     cout << "preintegration_p: " << frames[i].imu_process.pre_integration.delta_p.transpose() << endl;
    //     cout << "dist_p: " << dist_p.transpose() << endl;
    //     cout << "dist_v: " << dist_v.transpose() << endl;
    //     cout << endl;
    // }


}

void Initializer::RefineGravityVelocityPbc()
{
    Vector3d g0 = g.normalized() * G.norm();

    Vector3d b1, b2;

    Vector3d temp(1.0, 0.0, 0.0);
    if(temp == g.normalized())
        temp << 0.0, 0.0, 1.0;
    
    // b1 << g0(1) * temp(2) - g0(2) * temp(1), g0(0) * temp(2) - g0(2) * temp(0), g0(0) - temp(1) - g0(1) * temp(0);
    // b2 = b1.cross(b1);

    Vector3d a = g0.normalized();
    b1 = (temp - a * (a.transpose() * temp)).normalized();
    b2 = a.cross(b1);

    int n_state = frames.size() * 3 + 2 + 3;

    VectorXd x;
    
    int refine_times = 30;
    for(int k = 0;k < refine_times;k++)
    {
        MatrixXd A(n_state, n_state);
        A.setZero();
        VectorXd b(n_state);
        b.setZero();

        for(int i = 0;i < frames.size() - 1;i++)
        {
            Matrix<double, 6, 11> temp_A;
            temp_A.setZero();
            Matrix<double, 6, 1> temp_b;
            temp_b.setZero();

            double sum_dt = frames[i + 1].timestamp - frames[i].timestamp;
            temp_A.block<3, 3>(0, 0) = -Matrix3d::Identity() * sum_dt;
            temp_A.block<3, 3>(0, 3) = -Matrix3d::Zero();
            temp_A.block<3, 1>(0, 6) = 0.5 * Rbc * frames[i].R.transpose() * b1 * sum_dt * sum_dt;
            temp_A.block<3, 1>(0, 7) = 0.5 * Rbc * frames[i].R.transpose() * b2 * sum_dt * sum_dt;
            temp_A.block<3, 3>(0, 8) = Matrix3d::Identity() - Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
            temp_b.block<3, 1>(0, 0) = frames[i].imu_process.pre_integration.delta_p - Rbc * frames[i].R.transpose() * (frames[i + 1].P - frames[i].P)
                                        - 0.5 * Rbc * frames[i].R.transpose() * g0 * sum_dt * sum_dt;

            temp_A.block<3, 3>(3, 0) = -Matrix3d::Identity();
            temp_A.block<3, 3>(3, 3) = Rbc * frames[i].R.transpose() * frames[i + 1].R * Rbc.transpose();
            temp_A.block<3, 1>(3, 6) = Rbc * frames[i].R.transpose() * b1 * sum_dt;
            temp_A.block<3, 1>(3, 7) = Rbc * frames[i].R.transpose() * b2 * sum_dt;
            temp_b.block<3, 1>(3, 0) = frames[i].imu_process.pre_integration.delta_v - Rbc * frames[i].R.transpose() * g0 * sum_dt;
            
            Matrix<double, 6, 6> con_inv;
            con_inv.setIdentity();

            MatrixXd r_A = temp_A.transpose() * con_inv * temp_A;
            VectorXd r_b = temp_A.transpose() * con_inv * temp_b;

            A.block<6, 6>(i * 3, i * 3) += r_A.block<6, 6>(0, 0);
            A.bottomRightCorner<5, 5>() += r_A.block<5, 5>(6 ,6);
            A.block<6, 5>(i * 3, n_state - 5) += r_A.block<6, 5>(0, 6);
            A.block<5, 6>(n_state - 5, i * 3) += r_A.block<5, 6>(6, 0);
            
            b.block<6, 1>(i * 3, 0) += r_b.block<6, 1>(0, 0);
            b.block<5 ,1>(n_state - 5, 0) += r_b.block<5, 1>(6 ,0);

        }

        A = A * 1000.0;
        b = b * 1000.0;
        x = A.ldlt().solve(b);
        VectorXd dg = x.segment<2>(n_state - 5);
        g0 = (g0 + b1 * dg(0) + b2 * dg(1)).normalized() * G.norm();
    }

    g = g0;
    Pbc = x.segment<3>(n_state - 5);
    cout << "g_refine = " << g << endl;
    cout << "Pbc_refine = " << Pbc << endl;

    for(int i = 0;i < frames.size();i++)
    {
        frames[i].velocity = frames[i].R * Rbc.transpose() * x.segment<3>(i * 3);
        cout << "velocity:" << frames[i].velocity << endl;
    }
}

bool Initializer::CheckIMUObservibility()
{
    Vector3d sum_g;
    for (int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d tmp_g = frames[i].imu_process.pre_integration.delta_v / dt;
        sum_g += tmp_g;
    }
    Vector3d aver_g;
    aver_g = sum_g * 1.0 / ((int)frames.size() - 1);
    double var = 0;
    for(int i = 0;i < frames.size() - 1;i++)
    {
        double dt = frames[i + 1].timestamp - frames[i].timestamp;
        Vector3d tmp_g = frames[i].imu_process.pre_integration.delta_v / dt;
        var += (tmp_g - aver_g).transpose() * (tmp_g - aver_g);
        //cout << "frame g " << tmp_g.transpose() << endl;
    }
    var = sqrt(var / ((int)frames.size() - 1));
    //ROS_WARN("IMU variation %f!", var);
    cout << "var " << var << "=============================" << endl;
    if(var < 0.25)
    {
        frames.clear();
        return false;
    }
    else
        return true;
}

bool Initializer::initialize()
{
    ofstream ex_param_file;
    ex_param_file.open("/home/likaiyue/VI_Calibration/extrinsic_parameters.txt", ofstream::out | ofstream::app);   

    if(!ex_param_file)
    {
        printf("can not open the extrinsic_parameters.txt!\n");
        exit(-1);
    }

    // if(frames.size() < MIN_FRAME_NUM)
    //     return false;
    // if(!CheckIMUObservibility())
    //     return false;

    Vector3d Pbc_init;
    Matrix3d Rbc_init;
    Pbc_init << 0.0066314, -0.00816932, 0.03418203;

    Rbc_init << 0.99992988, -0.0070531, 0.00951262,
                0.00706146, 0.99997471, -0.00084483,
                -0.00950642, 0.00091194, 0.9999544;

    Rbc_init << 0.964451, 0.0862733, -0.249888,
                -0.0876095, 0.996141, 0.0057759,
                0.249422, 0.0163053, 0.968282;
    //Pbc << Pbc_init;

    Pbc << 0.0, 0.0, 0.0;
    
    if(!computeRbc())
        return false;
    else
    {
        exit(-1);
    }
    
    cout << "Rbc = " << endl;

    cout << Rbc << endl;
    cout << AngleAxisd(Rbc).angle() << endl;

    cout << AngleAxisd(Rbc_init).angle() << endl;
    

    //Rbc = Rbc_init;

    Rbc << 0.0148655429818, -0.999880929698, 0.00414029679422,
        0.999557249008, 0.0149672133247, 0.025715529948,
        -0.0257744366974, 0.00375618835797, 0.999660727178;
    
    computeBg();
    // //ComputeGravityScale_VIORB();
    // if(!computeGravityVelocity())
    //     return false;

    // EuRoc
    //Rbc.setIdentity();
    //computeBg();
    if(!computeGravityVelocity())
        return false;

    // g << 0.996686, -8.99173, -3.76769;

    ex_param_file << "------------------------------" << endl;
    ex_param_file << "scale:" << endl;
    ex_param_file << s << endl;
    ex_param_file << "Rbc:" << endl;
    ex_param_file << Rbc << endl;
    ex_param_file << "eulerAngle:" << endl;
    ex_param_file << Rbc.eulerAngles(2, 1, 0) << endl;
    ex_param_file << "first_Pbc:" << endl;
    ex_param_file << first_Pbc << endl;
    ex_param_file << "Pbc:" << endl;
    ex_param_file << Pbc << endl;
    ex_param_file << "first_g:" << endl;
    ex_param_file << first_g << endl;
    ex_param_file << "first_g.norm():" << endl;
    ex_param_file << first_g.norm() << endl;
    ex_param_file << "g:" << endl;
    ex_param_file << g << endl;
    ex_param_file << "imu_g:" << endl;

    Vector3d v(camera_front_rx, camera_front_ry, camera_front_rz);
    AngleAxisd aa(v.norm(), v.normalized());
    //ex_param_file << Rbc * aa.toRotationMatrix().transpose() * g << endl;
    ex_param_file << Rbc * frames[0].R.transpose() * g << endl;
    ex_param_file << "bg:" << endl;
    ex_param_file << bg << endl;

    ex_param_file.close();
    
    return true;
}